import src.model as model
import numpy as np
import pickle
import sklearn.cluster
import torch
import os
from src.utils.paths import DATA_PATH
from src.utils.utils import get_constant_function

EPS = 1e-10  # prevents numerical issues such as log(0) = nan

# Reproducibility
np.random.seed(1)
torch.manual_seed(1)

experiment_name = __file__[__file__.rfind(
        '/') + 1: -3]  # extract the filename between last `/' and the `.npy', i.e., the experiment's name

################################################################################
# Load data
X_obs = np.genfromtxt('../' + DATA_PATH + 'ionchannels/KcvMT325_140mV.asc')
X_obs = X_obs[None, 9200:14200]
t_obs = np.arange(0, X_obs.shape[1]) * 2e-4  # convert to seconds
t_span = [0, 1.0]

t_obs = t_obs[1:-1]  # throw away observations at boundaries
scaling_factor = 1.6e11
X_obs = X_obs[:, 1:-1] * scaling_factor  # rescale y-axis to prevent numerical issues


################################################################################
# Perform inference
n_states = 3
n_dim = 1
# Define model parameters
# Covariance: measurement error of current around 450-550 10-15 A; assume 500 fA to be the 2-sigma-threshold
obs_sig_unscaled = 500/2 * 1e-15
obs_sig_scaled = obs_sig_unscaled * scaling_factor
obs_cov = obs_sig_scaled ** 2
obs_cov = torch.tensor(np.array([[obs_cov]]), dtype=torch.float)

kmeans = sklearn.cluster.k_means(X_obs.reshape(-1, 1), n_clusters=n_states)

empirical_means, empirical_covs = [], []
clusters = []

# Find cluster mean and covariance (the latter is not readily available in kmeans object)
for i in range(n_states):
    clusters.append(X_obs[0, np.where(kmeans[1] == int(i))[0]])
    empirical_means.append([np.mean(clusters[-1])])
    empirical_covs.append([[np.std(clusters[-1])**2]])


initial_prior_mean = torch.as_tensor(empirical_means, dtype=torch.float)
initial_prior_cov = torch.as_tensor(empirical_covs, dtype=torch.float)

prior_slope = -1 * torch.ones((n_states, n_dim, n_dim))

prior_intercept = initial_prior_mean
prc_cov = 1e-4 * torch.as_tensor(np.array([np.eye(n_dim) for n in range(n_states)]), dtype=torch.float)

q = 100
prior_rate_mtx = q * torch.ones((n_states, n_states)) - q * torch.eye(n_states)
torch.diagonal(prior_rate_mtx)[:] = -1 * prior_rate_mtx.sum(1)

posterior_mean = get_constant_function(t_span, initial_prior_mean.numpy().flatten(), shape=(n_states, n_dim))
posterior_cov = get_constant_function(t_span, initial_prior_cov.numpy().flatten(), shape=(n_states, n_dim, n_dim))


S = model.VSHSSMF(t_span=t_span, t_obs=t_obs, obs_data=X_obs, obs_cov=obs_cov, prc_cov=prc_cov,
                  initial_prior_mean=initial_prior_mean, initial_prior_cov=initial_prior_cov,
                  initial_posterior_mean=initial_prior_mean, initial_posterior_cov=initial_prior_cov,
                  prior_slope=prior_slope, prior_intercept=prior_intercept, prior_rate_mtx=prior_rate_mtx,
                  posterior_mean=posterior_mean, posterior_cov=posterior_cov,
                  rate_clip=150, slope_clip=-1 * EPS, verbose=True)

opts_update_prior_params = dict(max_steps=10, max_iter=20, decay=0.5, epsrel_int=1e-2, epsrel_elbo=1e-3,
                                update_obs_cov=False, obs_cov_closed_form=True)

S.fit(max_iter=10, epsrel=1e-3, opts_update_prior_params=opts_update_prior_params,
      update_priors=True, checkpoint_name=experiment_name, checkpoint_interval=1)
################################################################################
# Save file itself, not included in output file

with open(__file__, 'r') as f:
    with open('../' + DATA_PATH + experiment_name + '/experiment_script_copy', 'w') as out:
        for line in (f.readlines()[:-7]):  # remove last 7 lines
            print(line, end='', file=out)
