import src.model as model
import numpy as np
import pickle
import sklearn.cluster
import torch
import os
from src.utils.paths import DATA_PATH
from src.utils.utils import get_constant_function

EPS = 1e-10  # prevents numerical issues such as log(0) = nan

# Reproducibility
np.random.seed(1)
torch.manual_seed(1)

experiment_name = __file__[__file__.rfind(
        '/') + 1: -3]  # extract the filename between last `/' and the `.npy', i.e., the experiment's name

################################################################################
# Define prior parameters and model

# Define potential
def U(x):
    """
    Potential as defined in Wu et al.

    Parameter
    ---------
    x, y : float
        2D-position
    """
    return (3 * torch.exp(-x[0]**2 - (x[1] - 3/3)**2) - 3 * torch.exp(-x[0]**2 - (x[1] - 5/3)**2)
            - 5 * torch.exp(-(x[0]-1.5)**2 - (x[1]+1/3)**2) - 5 * torch.exp(-(x[0]+1.5)**2 - (x[1]+1/3)**2) + 1/5 * x[0]**4 + 1/5 * (x[1] - 1/3)**4)

# Define potential gradient
def grad_U(x):
    """
    Gradient of potential.
    """
    dx = (3 * np.exp(-x[0]**2 - (x[1] - 1/3)**2) * -2 * x[0] - 3 * np.exp(-x[0]**2 - (x[1] - 5/3)**2) * -2 * x[0]
          -5 * np.exp(-(x[0]-1)**2 - x[1]**2) * -2 * (x[0]-1) - 5 * np.exp(-(x[0]+1)**2 - x[1]**2) * -2 * (x[0]+1)
          + 4/5 * x[0]**3)
    dy = (3 * np.exp(-x[0]**2 - (x[1] - 1/3)**2) * -2 * (x[1] - 1/3) - 3 * np.exp(-x[0]**2 - (x[1] - 5/3)**2) * -2 * (x[1] - 5/3)
          - 5 * np.exp(-(x[0]-1)**2 - x[1]**2) * -2 * x[1] - 5 * np.exp(-(x[0]+1)**2 - x[1]**2) * -2 * x[1]
          + 4/5 * (x[1] - 1/3)**3)
    res = np.array([dx, dy])
    return res

def euler_maruyama(y_init, t_interval, sigma, dt):
    t_start, t_end = t_interval
    sample_path = [y_init]
    t = [t_start]

    t_cur = t_start
    while t_cur < t_end:
        y_cur = sample_path[-1]
        y_next = y_cur - grad_U(y_cur) * dt + sigma @ np.random.multivariate_normal(np.zeros(2), dt * np.eye(2))

        t_cur += dt
        if t_cur <= t_end:
            sample_path.append(y_next)
            t.append(t_cur)

    return t, sample_path

t_span = [0, 20]

prc_sig = 1.1
prc_cov = prc_sig ** 2 * np.eye(2)

obs_cov = 0.2 * torch.eye(2)

dt = 0.001

################################################################################
# Perform forward simulation
try:
    with open('../' + DATA_PATH + experiment_name + '/forward.pkl', 'rb') as f:
        save_dict = pickle.load(f)


except:
    save_dict = None

if save_dict is None:

    N = 10
    t_all, y_all = [], []
    y_init = np.zeros(2)
    for i in range(N):
        t, y = euler_maruyama(y_init, t_span, prc_cov, dt)
        t_all.append(t)
        y_all.append(y)

    # Select one trajectory to perform inference on
    # Choose 3 or 8
    idx = 3
    #t_obs = np.around(sorted(np.random.uniform(high=t_span[1], size=100)), 3)

    # Draw observations as Poisson point process
    t_obs = np.arange(t_span[0], t_span[1], 0.2)
    t_obs = t_obs[1:-1]

    X_obs = []
    y = np.array(y_all[idx])
    t = np.array(t_all[idx])
    for elem in t_obs:
        t_ind = np.where(elem < t)[0][0]
        X_obs.append(y[t_ind] + np.random.multivariate_normal(mean=np.zeros(2), cov=obs_cov.numpy()))

    X_obs = np.array(X_obs).T
    # Save ground-truth data
    save_dict = dict(time_grid=t,
                     sde=y,
                     t_obs=t_obs,
                     X_obs=X_obs)
    if not os.path.exists('../' + DATA_PATH + experiment_name):
        os.makedirs('../' + DATA_PATH + experiment_name)

    with open('../' + DATA_PATH + experiment_name + '/forward.pkl', 'wb') as f:
        pickle.dump(save_dict, f)

t_obs = save_dict['t_obs']
X_obs = save_dict['X_obs']
################################################################################
# Perform inference
n_states = 3
n_dim = 2
# Define model parameters

kmeans = sklearn.cluster.k_means(X_obs.T, n_clusters=n_states)

empirical_means, empirical_covs = [], []
clusters = []

# Find cluster mean and covariance (the latter is not readily available in kmeans object)
for i in range(n_states):
    clusters.append(X_obs[:, np.where(kmeans[1] == int(i))[0]])
    empirical_means.append(np.mean(clusters[-1], axis=1))
    empirical_covs.append(np.cov(clusters[-1]))

initial_prior_mean = torch.as_tensor(empirical_means, dtype=torch.float)
initial_prior_cov = torch.as_tensor(empirical_covs, dtype=torch.float)

prior_slope = -1 * torch.as_tensor(np.array([np.eye(n_dim) for n in range(n_states)]), dtype=torch.float)

prior_intercept = initial_prior_mean
prc_cov = torch.tensor([initial_prior_cov.mean(0).numpy() for i in range(n_states)])

q = 1
prior_rate_mtx = q * torch.ones((n_states, n_states)) - q * torch.eye(n_states)
torch.diagonal(prior_rate_mtx)[:] = -1 * prior_rate_mtx.sum(1)

posterior_mean = get_constant_function(t_span, initial_prior_mean.numpy().flatten(), shape=(n_states, n_dim))
posterior_cov = get_constant_function(t_span, initial_prior_cov.numpy().flatten(), shape=(n_states, n_dim, n_dim))

S = model.VSHSSMF(t_span=t_span, t_obs=t_obs, obs_data=X_obs, obs_cov=obs_cov, prc_cov=prc_cov,
                  initial_prior_mean=initial_prior_mean, initial_prior_cov=initial_prior_cov,
                  initial_posterior_mean=initial_prior_mean, initial_posterior_cov=initial_prior_cov,
                  prior_slope=prior_slope, prior_intercept=prior_intercept, prior_rate_mtx=prior_rate_mtx,
                  posterior_mean=posterior_mean, posterior_cov=posterior_cov,
                  rate_clip=50, slope_clip=-1 * EPS, verbose=True)

opts_forward_backward_sweep = dict(max_iter=20, epsrel=1e-2, rtol=1e-3, max_steps_grad=10, max_iter_grad=20,
                                   decay=0.5)
opts_update_prior_params = dict(max_steps=10, max_iter=20, decay=0.5, epsrel_int=1e-2, epsrel_elbo=1e-3,
                                update_obs_cov=False, update_dispersion=True, mode_specific_dispersion=True)

S.fit(max_iter=50, opts_forward_backward_sweep=opts_forward_backward_sweep,
      opts_update_prior_params=opts_update_prior_params, epsrel=1e-3, update_priors=True,
      checkpoint_name=experiment_name, checkpoint_interval=10)
################################################################################
# Save file itself, not included in output file
# TODO create following directory if does not exist
with open(__file__, 'r') as f:
    with open('../' + DATA_PATH + experiment_name + '/experiment_script_copy', 'w') as out:
        for line in (f.readlines()[:-7]):  # remove last 7 lines
            print(line, end='', file=out)
