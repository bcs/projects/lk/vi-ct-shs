import src.model as model
import numpy as np
import pickle
import sklearn.cluster
import torch
import os
from src.utils.paths import DATA_PATH
from src.utils.utils import get_constant_function

EPS = 1e-10  # prevents numerical issues such as log(0) = nan

# Reproducibility
np.random.seed(1)
torch.manual_seed(1)

experiment_name = __file__[__file__.rfind(
        '/') + 1: -3]  # extract the filename between last `/' and the `.npy', i.e., the experiment's name

################################################################################
# Define prior parameters and model
n_states = 2
n_dim = 2
t_span = [0, 20]
obs_cov = 0.2 * torch.eye(n_dim)  # both modes have the same observation model
prc_cov = 0.7**2 * torch.tensor([np.eye(n_dim) for i in range(n_states)], dtype=torch.float)


alpha = torch.tensor([[[-0.1, 1.4],
                   [-2.6, 0.6]],
                  [[0.6, -1.4],
                   [2.6, 0.6]]], dtype=torch.float)
beta = torch.tensor([[8., 0.],
                     [-8., 0.]], dtype=torch.float)

model_slope = -1 * alpha
model_intercept = (alpha @ beta[..., None]).squeeze(2)

model_prior_mean = beta
model_prior_cov = prc_cov

model_initial_mjp = torch.tensor([1. - EPS, EPS])

q = 0.3
model_rate_mtx = torch.tensor([[-q, q],
                               [q, -q]])

S = model.VSHSSMF(t_span=t_span, obs_cov=obs_cov, prc_cov=prc_cov, initial_prior_mean=model_prior_mean,
                  initial_prior_cov=model_prior_cov, prior_slope=model_slope, prior_intercept=model_intercept,
                  initial_prior_mjp=model_initial_mjp, prior_rate_mtx=model_rate_mtx, verbose=True)

################################################################################
# Perform forward simulation
try:
    with open('../' + DATA_PATH + experiment_name + '/forward.pkl', 'rb') as f:
        save_dict = pickle.load(f)

except:
    save_dict = None

if save_dict is None:

    # Draw observations as Poisson point process
    t_obs = np.random.exponential(1 / 14, size=400)
    t_obs = np.cumsum(t_obs)
    t_obs = t_obs[t_obs <= t_span[1]]
    N_samples = 20
    observation_times = np.array([t_obs for i in range(N_samples)])

    trajectory = S.sample('prior', N_samples, observation_times)

    # Save ground-truth data
    save_dict = dict(time_grid=trajectory.time_grid,
                     sde=trajectory.sde,
                     mjp=trajectory.mjp_discretized,
                     t_obs=trajectory.observation_times,
                     X_obs=trajectory.observations)

    if not os.path.exists('../' + DATA_PATH + experiment_name):
        os.makedirs('../' + DATA_PATH + experiment_name)

    with open('../' + DATA_PATH + experiment_name + '/forward.pkl', 'wb') as f:
        pickle.dump(save_dict, f)

# Select one trajectory to perform inference on
idx = 1
t_obs = save_dict['t_obs'][idx]
X_obs = save_dict['X_obs'][idx].T
################################################################################
# Perform inference
n_states = 2
n_dim = 2
# Define model parameters

kmeans = sklearn.cluster.k_means(X_obs.T, n_clusters=n_states)

empirical_means, empirical_covs = [], []
clusters = []

# Find cluster mean and covariance (the latter is not readily available in kmeans object)
for i in range(n_states):
    clusters.append(X_obs[:, np.where(kmeans[1] == int(i))[0]])
    empirical_means.append(np.mean(clusters[-1], axis=1))
    empirical_covs.append(np.cov(clusters[-1]))

initial_prior_mean = torch.as_tensor(empirical_means, dtype=torch.float)
initial_prior_cov = torch.as_tensor(empirical_covs, dtype=torch.float)

prior_slope = -1 * torch.as_tensor(np.array([np.eye(n_dim) for n in range(n_states)]), dtype=torch.float)

prior_intercept = initial_prior_mean
prc_cov = 2 * torch.as_tensor(np.array([np.eye(n_dim) for n in range(n_states)]), dtype=torch.float)

q = 1
prior_rate_mtx = q * torch.ones((n_states, n_states)) - q * torch.eye(n_states)
torch.diagonal(prior_rate_mtx)[:] = -1 * prior_rate_mtx.sum(1)

posterior_mean = get_constant_function(t_span, initial_prior_mean.numpy().flatten(), shape=(n_states, n_dim))
posterior_cov = get_constant_function(t_span, initial_prior_cov.numpy().flatten(), shape=(n_states, n_dim, n_dim))

S = model.VSHSSMF(t_span=t_span, t_obs=t_obs, obs_data=X_obs, obs_cov=obs_cov, prc_cov=prc_cov,
                  initial_prior_mean=initial_prior_mean, initial_prior_cov=initial_prior_cov,
                  initial_posterior_mean=initial_prior_mean, initial_posterior_cov=initial_prior_cov,
                  prior_slope=prior_slope, prior_intercept=prior_intercept, prior_rate_mtx=prior_rate_mtx,
                  posterior_mean=posterior_mean, posterior_cov=posterior_cov,
                  rate_clip=50, slope_clip=-1 * EPS, verbose=True)

opts_forward_backward_sweep = dict(max_iter=50, epsrel=1e-2, rtol=1e-3, max_steps_grad=10, max_iter_grad=20,
                                   decay=0.2)

opts_update_prior_params = dict(max_steps=10, max_iter=20, decay=0.5, epsrel_int=1e-2, epsrel_elbo=1e-3,
                                update_obs_cov=False, obs_cov_closed_form=True)

S.fit(max_iter=50, epsrel=1e-3, update_priors=True, opts_forward_backward_sweep=opts_forward_backward_sweep,
      opts_update_prior_params=opts_update_prior_params,
      checkpoint_name=experiment_name, checkpoint_interval=1)
################################################################################
# Save file itself, not included in output file

with open(__file__, 'r') as f:
    with open('../' + DATA_PATH + experiment_name + '/experiment_script_copy', 'w') as out:
        for line in (f.readlines()[:-7]):  # remove last 7 lines
            print(line, end='', file=out)
