import src.model as model
import numpy as np
import pickle
import sklearn.cluster
import torch
import os
from src.utils.paths import DATA_PATH
from src.utils.utils import get_constant_function

EPS = 1e-10  # prevents numerical issues such as log(0) = nan

# Reproducibility
np.random.seed(1)
torch.manual_seed(1)

experiment_name = __file__[__file__.rfind(
        '/') + 1: -3]  # extract the filename between last `/' and the `.npy', i.e., the experiment's name

################################################################################
# Define prior parameters and model

# Define potential
def V(x, magnitude=1, scale_quartic=1, scale_squared=1):
    return 4 * (x**8 + 3 * np.exp(-80 * x**2) + 2 * np.exp(-80 * (x - 0.5)**2) + 2 * np.exp(-80 * (x + 0.5)**2))

# Define potential gradient
def grad_V(x, magnitude=1, scale_quartic=1, scale_squared=1):
    return 4 * (8 * x**7 + 3 * np.exp(-80 * x**2) * (-160 * x) + 2.5 * np.exp(-80 * (x - 0.5)**2) * (-160 * (x - 0.5))
                + 2.5 * np.exp(-80 * (x + 0.5)**2) * (-160 * (x + 0.5)))

def euler_maruyama(y_init, t_interval, sigma, dt):
    t_start, t_end = t_interval
    sample_path = [y_init]
    t = [t_start]

    t_cur = t_start
    while t_cur < t_end:
        y_cur = sample_path[-1]
        y_next = y_cur - grad_V(y_cur, scale_quartic=.4, scale_squared=1.2) * dt + sigma * np.random.normal(scale=np.sqrt(dt))

        t_cur += dt
        if t_cur <= t_end:
            sample_path.append(y_next)
            t.append(t_cur)

    return t, sample_path

t_span = [0, 20]

prc_sig = 1.9
prc_cov = prc_sig ** 2

obs_sig = 0.15
obs_cov = obs_sig**2

dt = 0.001

################################################################################
# Perform forward simulation
N = 10
t_all, y_all = [], []
for i in range(N):
    t, y = euler_maruyama(np.random.normal(), t_span, prc_sig, dt)
    t_all.append(t)
    y_all.append(y)

# Select one trajectory to perform inference on
idx = 5

# Draw observations as Poisson point process
t_obs = np.arange(t_span[0], t_span[1], 0.2)
t_obs = t_obs[1:-1]

X_obs = []
y = y_all[idx]
t = t_all[idx]
for elem in t_obs:
    #t_ind = np.where(np.isclose(t, elem))[0][0]
    t_ind = np.where(elem < t)[0][0]
    X_obs.append(y[t_ind] + np.random.normal(scale=obs_sig))

X_obs = np.array(X_obs)[None, :]
# Save ground-truth data
save_dict = dict(time_grid=t,
                 sde=y,
                 t_obs=t_obs,
                 X_obs=X_obs)

if not os.path.exists('../' + DATA_PATH + experiment_name):
    os.makedirs('../' + DATA_PATH + experiment_name)

with open('../' + DATA_PATH + experiment_name + '/forward.pkl', 'wb') as f:
    pickle.dump(save_dict, f)

################################################################################
# Perform inference
n_states = 4
n_dim = 1
# Define model parameters
obs_cov = obs_sig**2 * torch.eye(1)

kmeans = sklearn.cluster.k_means(X_obs.reshape(-1, 1), n_clusters=4)

empirical_means, empirical_covs = [], []
clusters = []

# Find cluster mean and covariance (the latter is not readily available in kmeans object)
for i in range(n_states):
    clusters.append(X_obs[0, np.where(kmeans[1] == int(i))[0]])
    empirical_means.append([np.mean(clusters[-1])])
    empirical_covs.append([[np.std(clusters[-1])**2]])


initial_prior_mean = torch.as_tensor(empirical_means, dtype=torch.float)
initial_prior_cov = torch.as_tensor(empirical_covs, dtype=torch.float)

prior_slope = -1 * torch.ones((n_states, n_dim, n_dim))

prior_intercept = initial_prior_mean
prc_cov = initial_prior_cov

q = 1
prior_rate_mtx = q * torch.ones((n_states, n_states)) - q * torch.eye(n_states)
torch.diagonal(prior_rate_mtx)[:] = -1 * prior_rate_mtx.sum(1)

posterior_mean = get_constant_function(t_span, initial_prior_mean.numpy().flatten(), shape=(n_states, n_dim))
posterior_cov = get_constant_function(t_span, initial_prior_cov.numpy().flatten(), shape=(n_states, n_dim, n_dim))

S = model.VSHSSMF(t_span=t_span, t_obs=t_obs, obs_data=X_obs, obs_cov=obs_cov, prc_cov=prc_cov,
                  initial_prior_mean=initial_prior_mean, initial_prior_cov=initial_prior_cov,
                  initial_posterior_mean=initial_prior_mean, initial_posterior_cov=initial_prior_cov,
                  prior_slope=prior_slope, prior_intercept=prior_intercept, prior_rate_mtx=prior_rate_mtx,
                  posterior_mean=posterior_mean, posterior_cov=posterior_cov,
                  rate_clip=50, slope_clip=-1 * EPS, verbose=True)

opts_update_prior_params = dict(max_steps=10, max_iter=20, decay=0.5, epsrel_int=1e-2, epsrel_elbo=1e-3,
                                update_obs_cov=False)

S.fit(max_iter=10, epsrel=1e-3, opts_update_prior_params=opts_update_prior_params,
      update_priors=True, checkpoint_name=experiment_name, checkpoint_interval=1)
################################################################################
# Save file itself, not included in output file

with open(__file__, 'r') as f:
    with open('../' + DATA_PATH + experiment_name + '/experiment_script_copy', 'w') as out:
        for line in (f.readlines()[:-7]):  # remove last 7 lines
            print(line, end='', file=out)
