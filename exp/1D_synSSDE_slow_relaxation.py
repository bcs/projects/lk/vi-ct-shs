import src.model as model
import numpy as np
import pickle
import sklearn.cluster
import torch
import os
from src.utils.paths import DATA_PATH
from src.utils.utils import get_constant_function

EPS = 1e-10  # prevents numerical issues such as log(0) = nan

# Reproducibility
np.random.seed(1)
torch.manual_seed(1)

experiment_name = __file__[__file__.rfind(
        '/') + 1: -3]  # extract the filename between last `/' and the `.npy', i.e., the experiment's name

################################################################################
# Define prior parameters and model
n_states = 2
n_dim = 1
t_span = [0, 20]
obs_cov = 0.1 * torch.eye(1)  # both modes have the same observation model
prc_cov = 0.5 ** 2 * torch.tensor([np.eye(n_dim) for i in range(n_states)], dtype=torch.float)
model_prior_mean = torch.tensor([[1.], [-1.]])
model_prior_cov = 0.2 * torch.tensor([[[1.]], [[1.]]])

alpha = torch.tensor([[1.], [-1.]])
beta = torch.tensor([[[0.4]], [[0.4]]])
model_slope = -1 * beta
model_intercept = (beta @ alpha[..., None]).squeeze(2)

model_initial_mjp = torch.tensor([1. - EPS, EPS])

q = 0.2
model_rate_mtx = torch.tensor([[-q, q],
                               [q, -q]])

S = model.VSHSSMF(t_span=t_span, obs_cov=obs_cov, prc_cov=prc_cov, initial_prior_mean=model_prior_mean,
                  initial_prior_cov=model_prior_cov, prior_slope=model_slope, prior_intercept=model_intercept,
                  initial_prior_mjp=model_initial_mjp, prior_rate_mtx=model_rate_mtx, verbose=True)

################################################################################
# Perform forward simulation (if no data exists yet)
try:
    with open('../' + DATA_PATH + experiment_name + '/forward.pkl', 'rb') as f:
        save_dict = pickle.load(f)


except:
    save_dict = None

if save_dict is None:
    N_samples = 10

    # Draw observations as Poisson point process
    t_obs = np.random.exponential(0.35, size=100)
    t_obs = np.cumsum(t_obs)
    t_obs = t_obs[t_obs <= t_span[1]]
    N_samples = 20
    observation_times = np.array([t_obs for i in range(N_samples)])

    trajectory = S.sample('prior', N_samples, observation_times)

    # Select one trajectory to perform inference on
    idx = 8

    # Save ground-truth data
    save_dict = dict(time_grid=trajectory.time_grid,
                     sde=trajectory.sde[idx],
                     mjp=trajectory.mjp_discretized[idx],
                     t_obs=trajectory.observation_times[idx],
                     X_obs=trajectory.observations[idx])

    if not os.path.exists('../' + DATA_PATH + experiment_name):
        os.makedirs('../' + DATA_PATH + experiment_name)

    with open('../' + DATA_PATH + experiment_name + '/forward.pkl', 'wb') as f:
        pickle.dump(save_dict, f)

t_obs = save_dict['t_obs']
X_obs = save_dict['X_obs']
################################################################################
# Perform inference
# Define model parameters
kmeans = sklearn.cluster.k_means(X_obs.reshape(-1, 1), n_clusters=2)

empirical_means, empirical_covs = [], []
clusters = []

for i in range(n_states):
    clusters.append(X_obs[0, np.where(kmeans[1] == int(i))[0]])
    empirical_means.append([np.mean(clusters[-1])])
    empirical_covs.append([[np.std(clusters[-1])**2]])

initial_prior_mean = torch.as_tensor(empirical_means[::-1], dtype=torch.float)
initial_prior_cov = torch.as_tensor(empirical_covs[::-1], dtype=torch.float)
obs_cov = initial_prior_cov.mean(0)

prior_slope = -1 * torch.tensor([[[1.]], [[1.]]], dtype=torch.float)

prior_intercept = initial_prior_mean
prc_cov = torch.ones(size=(n_states, n_dim, n_dim), dtype=torch.float)
prc_cov[0] = initial_prior_cov.mean(0)
prc_cov[1] = initial_prior_cov.mean(0)

q = 1
prior_rate_mtx = torch.tensor([[-q, q],
                               [q, -q]])

initial_prior_mjp = torch.tensor([0.5, 0.5])

posterior_mean = get_constant_function(t_span, initial_prior_mean.numpy().flatten(), shape=(n_states, n_dim))
posterior_cov = get_constant_function(t_span, initial_prior_cov.numpy().flatten(), shape=(n_states, n_dim, n_dim))

S = model.VSHSSMF(t_span=t_span, t_obs=t_obs, obs_data=X_obs, obs_cov=obs_cov, prc_cov=prc_cov,
                  initial_prior_mean=initial_prior_mean, initial_prior_cov=initial_prior_cov, prior_slope=prior_slope,
                  prior_intercept=prior_intercept, initial_prior_mjp=initial_prior_mjp, prior_rate_mtx=prior_rate_mtx,
                  posterior_mean=posterior_mean, posterior_cov=posterior_cov,
                  initial_posterior_mean=initial_prior_mean, initial_posterior_cov=initial_prior_cov,
                  rate_clip=50, slope_clip=-1 * EPS, verbose=True)

opts_update_prior_params = dict(max_steps=10, max_iter=20, decay=0.5, epsrel_int=1e-2, epsrel_elbo=1e-3,
                                update_obs_cov=True, obs_cov_closed_form=True)

S.fit(max_iter=10, epsrel=1e-3, update_priors=True, opts_update_prior_params=opts_update_prior_params,
      checkpoint_name=experiment_name, checkpoint_interval=10)
################################################################################
# Save file itself, not included in output file

with open(__file__, 'r') as f:
    with open('../' + DATA_PATH + experiment_name + '/experiment_script_copy', 'w') as out:
        for line in (f.readlines()[:-7]):  # remove last 7 lines
            print(line, end='', file=out)
