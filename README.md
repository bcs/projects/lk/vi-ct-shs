# Variational Inference for Continuous-Time Switching Dynamical Systems

Code for the NeurIPS 2021 publication "Variational Inference for Continuous-Time Switching Dynamical Systems" by Lukas Köhs, Bastian Alt, and Heinz Koeppl.

### Installation

To install required packages, run

```setup
pip install -r requirements.txt
```

Code has been developed and tested using python 3.6.8.

### Project structure

```/exp```
Experiment files. Correspondence to figures is as follows:
- Fig 2A: 1D_synSSDE.py
- Fig 2B: 1D_4well.py
- Fig 3: 2D_3well.py
- Fig 4: 1D_ionchannel.py
- Fig 5: 2D_swirls.py

```/src```
Source files for the model and plotting.

```/data```
Holds ion channel measurement data; all other data are synthetic and automatically generated during the experiments.
