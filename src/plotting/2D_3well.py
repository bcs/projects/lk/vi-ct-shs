import numpy as np
import matplotlib
from matplotlib import cm
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.patches import Ellipse
import matplotlib.transforms as transforms
import pickle
import scipy.stats as st
import torch
from src.utils.paths import DATA_PATH, CHECKPOINT_PATH, PLOTS_PATH

matplotlib.rcParams['axes.linewidth'] = 1.5 # set the value globally
matplotlib.rcParams['mathtext.fontset'] = 'cm'
matplotlib.rcParams['mathtext.rm'] = 'serif'

def confidence_ellipse(cov, mu, ax, n_std=3.0, facecolor='none', **kwargs):
    """
    Create a plot of the covariance confidence ellipse of *x* and *y*.

    Parameters
    ----------
    cov : array-like, shape (2, 2)
        Covariance matrix.

    ax : matplotlib.axes.Axes
        The axes object to draw the ellipse into.

    n_std : float
        The number of standard deviations to determine the ellipse's radiuses.

    Returns
    -------
    matplotlib.patches.Ellipse

    Other parameters
    ----------------
    kwargs : `~matplotlib.patches.Patch` properties
    """
    pearson = cov[0, 1] / np.sqrt(cov[0, 0] * cov[1, 1])
    # Using a special case to obtain the eigenvalues of this
    # two-dimensionl dataset.
    ell_radius_x = np.sqrt(1 + pearson)
    ell_radius_y = np.sqrt(1 - pearson)
    ellipse = Ellipse((0, 0),
                      width=ell_radius_x * 2,
                      height=ell_radius_y * 2,
                      facecolor=facecolor,
                      **kwargs)

    # NOTE to self: in the following lines, I double-checked that this procedure indeed gives the correct result
    # via the more conventional computation of eigenvalues/-vectors
    #import numpy.linalg as la
    #w, v = la.eig(cov)
    #ellipse1 = Ellipse((0, 0), width=n_std * np.sqrt(w[0])*2, height=n_std * np.sqrt(w[1])*2, facecolor='yellow', **kwargs)
    #theta = np.arccos(v[0, 0]/np.sqrt(np.sum(v[:, 0]**2)))
    #transf = transforms.Affine2D().rotate(-theta).translate(mu[0], mu[1])
    #ellipse1.set_transform(transf + ax.transData)
    #ax.add_patch(ellipse1)

    # Calculating the stdandard deviation of x from
    # the squareroot of the variance and multiplying
    # with the given number of standard deviations.
    scale_x = n_std * np.sqrt(cov[0, 0])
    mean_x = mu[0]

    # calculating the stdandard deviation of y ...
    scale_y = n_std * np.sqrt(cov[1, 1])
    mean_y = mu[1]

    transf = transforms.Affine2D() \
        .rotate_deg(45) \
        .scale(scale_x, scale_y) \
        .translate(mean_x, mean_y)

    ellipse.set_transform(transf + ax.transData)
    return ax.add_patch(ellipse)


# -------------------- Loading --------------------
# Get experiment name (file names have to match)
experiment_name = __file__[__file__.rfind(
        '/') + 1: -3]  # extract the filename between last `/' and the `.npy', i.e., the experiment's name

# Load synthetic data
with open('../../' + DATA_PATH + experiment_name + '/forward.pkl', 'rb') as f:
        trajectory = pickle.load(f)

time_grid = trajectory['time_grid']
diffusion_path = trajectory['sde']
X_obs = trajectory['X_obs']
t_obs = trajectory['t_obs']

# Load VSHSSMF object
#timestamp = '20210517-154422'
timestamp = '20211003-134301'
S = torch.load('../../' + CHECKPOINT_PATH + f'vssde_fit_2D_3well_learn_params_no_obscov_D_z_eqspace_obs_{timestamp}_iter_final.pt')  # TODO


# -------------------- Plotting --------------------
q_z = S.posterior_mjp(time_grid)
mu = S.posterior_mean(time_grid)
Sigma = S.posterior_cov(time_grid)
mu_avg = (q_z[:, None, :] * mu).sum(0)

# Plot three things:
#  1. The ground-truth diffusion trajectory with observations
#  2. The posterior marginals q(z) for all modes z
#  3. The map path (TODO phase or time course?)
fig, ax = plt.subplots(2, sharex=True, gridspec_kw={'hspace': 0.1, 'height_ratios': [1, 5]}, figsize=(5, 3.2))
#fig = plt.figure(figsize=(8, 5))
#gs = gridspec.GridSpec(nrows=2, ncols=1, height_ratios=[5, 1])

# 1. Z-reconstruction
#ax0 = fig.add_subplot(gs[0, 0])
ax[0].plot(time_grid, q_z[0], c='blue', lw=2)
ax[0].plot(time_grid, q_z[1], c='red', lw=2)
ax[0].plot(time_grid, q_z[2], c='green', lw=2)
ax[0].set_yticks([0, 1])
ax[0].set_ylabel('$q_Z$', fontsize='x-large')
ax[0].yaxis.set_label_coords(-0.04, 0.5)

# 2. Y-reconstruction
MAP_indices = []
for t_i, t in enumerate(time_grid):
    st_objects = [st.multivariate_normal(mean=mu[z, :, t_i], cov=Sigma[z, :, :, t_i]) for z in range(S.n_states)]
    density_at_mean = np.array([density.pdf(mu[:, :, t_i]) for density in st_objects])
    weighted_density_at_mean = q_z[:, t_i, None] * density_at_mean

    overall_density_at_mean = np.sum(weighted_density_at_mean, axis=0)
    MAP_indices.append(np.argmax(overall_density_at_mean))


MAP_indices = np.array(MAP_indices)
MAP = np.array([mu[MAP_indices[i], :, i] for i in range(time_grid.shape[0])])

ax[1].plot(time_grid, diffusion_path[:, 0], alpha=.4, lw=.8, c='tab:blue')
ax[1].plot(time_grid, diffusion_path[:, 1], alpha=.4, lw=.8, c='tab:orange')
ax[1].plot(time_grid, MAP[:, 0], label='$Y_1$', c='tab:blue', lw=2)
ax[1].plot(time_grid, MAP[:, 1], label='$Y_2$', c='tab:orange', lw=2)
ax[1].scatter(t_obs, X_obs[0], alpha=.6, marker='x', c='tab:blue')
ax[1].scatter(t_obs, X_obs[1], alpha=.6, marker='x', c='tab:orange')
ax[1].set_ylabel('$Y$', fontdict={'size': 'x-large', 'name': 'cmr10'})
ax[1].set_xlabel('Time', fontdict={'size': 'x-large', 'name': 'cmr10'})
ax[1].yaxis.set_label_coords(-0.04, 0.5)
ax[1].set_xticks([0, 5, 10, 15, 20])
ax[1].set_xlim([0, 20])
ax[1].set_yticks([-1, 0, 1, 2])

# Shade area where q(z, t) is not (relatively) unambiguous
ax[0].axvspan(11.0, 11.85, alpha=0.5, color='gray')
ax[1].axvspan(11.0, 11.85, alpha=0.5, color='gray')

# Place legend
ax[1].legend(loc='center right', bbox_to_anchor=(0.87, 0.85), fontsize='large')
plt.subplots_adjust(left=0.08, right=0.98, top=0.98, bottom=0.14)

plt.savefig('../../' + PLOTS_PATH + experiment_name + f'/overview_{timestamp}.pdf', format='pdf')
plt.close()


# Plot the potential landscape, the observations and the MAP path as phase plot
def U(x, y):
    """
    Potential as defined in Wu et al.

    Parameter
    ---------
    x, y : float
        2D-position
    """
    return (3 * np.exp(-x**2 - (y - 1/3)**2) - 3 * np.exp(-x**2 - (y - 5/3)**2)
            - 5 * np.exp(-(x-1)**2 - (y + 0)**2) - 5 * np.exp(-(x+1)**2 - (y+0)**2) + 1/5 * x**4 + 1/5 * (y - 1/3)**4)

x = np.linspace(-2.5, 2.5, 200)
y = np.linspace(-2, 2.2, 200)
X, Y = np.meshgrid(x, y)

fig, ax = plt.subplots(1, figsize=(5, 3.2))
ax.contourf(X, Y, U(X, Y), levels=25, cmap=cm.inferno)
colors = ['blue', 'red', 'green']
for t_i, t in enumerate(t_obs):
    t_index = np.argmin(np.abs(time_grid - t))
    q_max = np.argmax(q_z[:, t_index])
    ax.scatter(X_obs[0, t_i], X_obs[1, t_i], c=colors[int(q_max)], alpha=.9, marker='x', s=80)

# Plot prior means and reconstructed dispersions
mu_0 = -1 * np.linalg.inv(S.prior_slope[0].detach().numpy()) @ S.prior_intercept[0].detach().numpy()
mu_1 = -1 * np.linalg.inv(S.prior_slope[1].detach().numpy()) @ S.prior_intercept[1].detach().numpy()
mu_2 = -1 * np.linalg.inv(S.prior_slope[2].detach().numpy()) @ S.prior_intercept[2].detach().numpy()
ax.scatter(*mu_0, marker='D', s=90, c='blue')
confidence_ellipse(S.prc_cov[0].detach().numpy(), mu_0, ax, n_std=3, lw=2, edgecolor='blue')
ax.scatter(*mu_1, marker='D', s=90, c='red')
confidence_ellipse(S.prc_cov[1].detach().numpy(), mu_1, ax, n_std=3, lw=2, edgecolor='red')
ax.scatter(*mu_2, marker='D', s=90, c='green')
confidence_ellipse(S.prc_cov[2].detach().numpy(), mu_2, ax, n_std=3, lw=2, edgecolor='green')
ax.set_yticks([-2, -1, 0, 1, 2])
ax.set_xlabel('$Y_1$', fontdict={'size': 'x-large'})
ax.set_ylabel('$Y_2$', fontsize='x-large')
ax.yaxis.set_label_coords(-0.05, 0.5)
ax.xaxis.set_label_coords(0.5, -0.08)

plt.subplots_adjust(left=0.09, right=0.99, top=0.99, bottom=0.13)

plt.savefig('../../' + PLOTS_PATH + experiment_name + f'/cluster_assignment_{timestamp}.pdf', format='pdf')
plt.close()

