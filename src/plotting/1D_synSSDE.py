import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pickle
import scipy.stats as st
import torch
from src.utils.paths import DATA_PATH, CHECKPOINT_PATH, PLOTS_PATH

matplotlib.rcParams['axes.linewidth'] = 1.5 # set the value globally
matplotlib.rcParams['mathtext.fontset'] = 'cm'
matplotlib.rcParams['mathtext.rm'] = 'serif'

# -------------------- Loading --------------------
# Get experiment name (file names have to match)
experiment_name = __file__[__file__.rfind(
        '/') + 1: -3]  # extract the filename between last `/' and the `.npy', i.e., the experiment's name

# Load synthetic data
with open('../../' + DATA_PATH + experiment_name + '/forward.pkl', 'rb') as f:
        trajectory = pickle.load(f)

time_grid = trajectory['time_grid']
diffusion_path = trajectory['sde']
jump_path = trajectory['mjp']

# Load VSHSSMF object
timestamp = '20211001-130148'
S = torch.load('../../' + CHECKPOINT_PATH + f'vssde_fit_1D_synSSDE_learn_params_all_Poisson_obs_{timestamp}_iter_final.pt')  # TODO


# -------------------- Plotting --------------------
q_z = S.posterior_mjp(time_grid)
mu = S.posterior_mean(time_grid)
Sigma = S.posterior_cov(time_grid)
mu_avg = (q_z[:, None, :] * mu).sum(0).squeeze(0)

# Plot three things:
#  1. The ground-truth MJP trajectory
#  2. The posterior marginal q(Z=0) (which hopefully resembles the ground-truth MJP trajectory)
#  3. The posterior means mu(z, t) with their 1-sigma-tubes with some posterior diffusion trajectories
fig, ax = plt.subplots(4, sharex=True, gridspec_kw={'hspace': 0.1, 'height_ratios': [4, 1, 4, 19]}, figsize=(5, 3.5))
# 1. Ground-truth MJP trajectory
ax[0].plot(time_grid, jump_path, c='blue', lw=2)
ax[0].set_yticks([0, 1])
ax[0].set_yticklabels(['1', '2'])
ax[0].set_ylabel('$Z$', fontsize='x-large')
ax[0].yaxis.set_label_coords(-0.06, 0.5)

# 2. Plot observation times as vertical lines
for t in S.t_obs:
    ax[1].axvline(t, lw=0.5, c='k')
ax[1].tick_params(axis='y', left=False, labelleft=False)
ax[1].tick_params(axis='x', bottom=False, labelbottom=False)
ax[1].spines['top'].set_visible(False)
ax[1].spines['left'].set_visible(False)
ax[1].spines['right'].set_visible(False)
ax[1].spines['bottom'].set_visible(False)


# 3. q(Z=0) reconstruction
ax[2].plot(time_grid, q_z[0], c='blue', lw=2)
ax[2].set_ylabel('$q_Z$', fontsize='x-large')
ax[2].set_yticks([0, 1])
ax[2].yaxis.set_label_coords(-0.06, 0.5)
# 2.a. plot MAP-path
ax[2].plot(time_grid, np.argmin(q_z, axis=0), ls='--', lw=2, c='tab:orange')

# 3. Diffusion plot
x = np.linspace(-2, 2.5, 1000)
grid_midpoints = x[:-1] + 0.5 * np.diff(x)
mat = []
for t_i, t in enumerate(time_grid):
    mat.append(q_z[0, t_i] * st.norm(loc=mu[0, 0, t_i], scale=np.sqrt(Sigma[0, 0, 0, t_i])).pdf(grid_midpoints)
               + q_z[1, t_i] * st.norm(loc=mu[1, 0, t_i], scale=np.sqrt(Sigma[1, 0, 0, t_i])).pdf(grid_midpoints))
mat = np.array(mat)
ax[3].matshow(mat.T, cmap='hot', aspect='auto', origin='lower', extent=(0, 20, -2, 2.5))
ax[3].xaxis.set_ticks_position('bottom')

# Overlay the true latent trajectory and the observations
ax[3].scatter(S.t_obs, S.obs_data[0], marker='x', c='white', s=60, zorder=10)
ax[3].plot(time_grid, diffusion_path[0], lw=.7, alpha=.6, c='white')

# Overlay the MAP-path
MAP_indices = []
for t_i, t in enumerate(time_grid):
    st_objects = [st.norm(loc=mu[z, :, t_i], scale=np.sqrt(Sigma[z, :, :, t_i])) for z in range(S.n_states)]
    density_at_mean = np.array([density.pdf(mu[:, :, t_i]) for density in st_objects])
    weighted_density_at_mean = q_z[:, t_i, None] * density_at_mean

    overall_density_at_mean = np.sum(weighted_density_at_mean, axis=0)
    MAP_indices.append(np.argmax(overall_density_at_mean))

MAP_indices = np.array(MAP_indices)
MAP = np.array([mu[MAP_indices[i], :, i] for i in range(time_grid.shape[0])])
ax[3].plot(time_grid, MAP[:, 0], c='tab:orange', lw=2, ls='--')
ax[3].set_xlabel('Time', fontdict={'size': 'x-large', 'name': 'cmr10'})
ax[3].set_ylabel('$Y$', fontsize='x-large')
ax[3].yaxis.set_label_coords(-0.06, 0.5)

# First, plot scale bar
ax[3].annotate('', xy=(S.t_obs[39], -1.4), xytext=(S.t_obs[40], -1.4),
             arrowprops={'arrowstyle': '|-|,widthA=0.2,widthB=0.2', 'lw': 1, 'color': 'white'},
             va='center', transform=ax[2].transData, color='white', horizontalalignment='center')
# Then add text
ax[3].annotate('$\Delta t_{\mathrm{trans}}$', xy=(S.t_obs[39] + 0.5 * (S.t_obs[40] - S.t_obs[39]), -1.4), xycoords='data',
               xytext=(0, -12), ha='center', textcoords='offset points', color='white', fontsize='large')

plt.subplots_adjust(left=0.10, right=0.98, top=0.985, bottom=0.13)

plt.savefig('../../' + PLOTS_PATH + experiment_name + f'/overview_{timestamp}.pdf', format='pdf')
plt.close()

# Plot posterior sample trajectories
n_trajectories = 10
trajectory_posterior = S.sample(process='posterior', n_trajectories=n_trajectories)

fig, ax = plt.subplots(1, figsize=(5, 3))
ax.plot(time_grid, diffusion_path[0], c='b')
for i in range(n_trajectories):
        ax.plot(trajectory_posterior.time_grid, trajectory_posterior.sde[i, 0], color='dimgray', alpha=.4, lw=.7)

ax.set_xlabel('Time', fontdict={'size': 'x-large', 'name':'cmr10'})
ax.set_ylabel('$Y$', fontsize='x-large')
ax.set_ylim([-2, 2.5])
ax.set_yticks([-2, -1, 0, 1, 2])
ax.yaxis.set_label_coords(-0.05, 0.5)

plt.subplots_adjust(left=0.09, right=0.99, top=0.99, bottom=0.15)

plt.savefig('../../' + PLOTS_PATH + experiment_name + f'/posterior_samples_{timestamp}.pdf', format='pdf')
plt.close()

# Plot the individual modes for SI:
#  1. The posterior marginal q(Z=0) (which hopefully resembles the ground-truth MJP trajectory)
#  2. The posterior means mu(z, t) with their 1-sigma-tubes with some posterior diffusion trajectories
fig, ax = plt.subplots(2, sharex=True, gridspec_kw={'hspace': 0.1, 'height_ratios': [4, 19]}, figsize=(5, 3.5))

# 1. q(Z=0) reconstruction
ax[0].plot(time_grid, q_z[0], c='blue', lw=2)
ax[0].set_ylabel('$q_Z$', fontsize='x-large')
ax[0].set_yticks([0, 1])
ax[0].yaxis.set_label_coords(-0.06, 0.5)
# 1.a. plot MAP-path
ax[0].plot(time_grid, np.argmin(q_z, axis=0), ls='--', lw=2, c='tab:orange')

# 2. Diffusion plot
ax[1].plot(time_grid, mu[0, 0], c='tab:red')
ax[1].fill_between(time_grid, mu[0, 0] - np.sqrt(Sigma[0,0,0]), mu[0,0] + np.sqrt(Sigma[0,0,0]), color='tab:red', alpha=.4)
ax[1].plot(time_grid, mu[1, 0], c='tab:green')
ax[1].fill_between(time_grid, mu[1, 0] - np.sqrt(Sigma[1,0,0]), mu[1,0] + np.sqrt(Sigma[1,0,0]), color='tab:green', alpha=.4)
ax[1].plot(time_grid, MAP[:, 0], c='tab:orange', lw=2, ls='--')
ax[1].xaxis.set_ticks_position('bottom')

# Overlay the true latent trajectory and the observations
ax[1].scatter(S.t_obs, S.obs_data[0], marker='x', c='k', s=60, zorder=10)
ax[1].plot(time_grid, diffusion_path[0], lw=.7, alpha=.6, c='k')

ax[1].set_xlabel('Time', fontdict={'size': 'x-large', 'name': 'cmr10'})
ax[1].set_ylabel('$Y$', fontsize='x-large')
ax[1].yaxis.set_label_coords(-0.06, 0.5)

plt.subplots_adjust(left=0.10, right=0.98, top=0.985, bottom=0.13)

plt.savefig('../../' + PLOTS_PATH + experiment_name + f'/individual_modes_{timestamp}.pdf', format='pdf')
plt.close()

# Plot the ELBO over iterations
fig, ax = plt.subplots(1, figsize=(5, 3))
elbo_history = np.array(S.elbo_history)
ax.plot(np.arange(1, len(elbo_history) + 1), elbo_history)
ax.set_xlabel('Iteration', fontdict={'size': 'x-large', 'name': 'cmr10'})
ax.set_ylabel('ELBO', fontsize='x-large')
plt.subplots_adjust(left=0.13, right=0.98, top=0.985, bottom=0.15)
plt.savefig('../../' + PLOTS_PATH + experiment_name + f'/ELBO_v_iterations_{timestamp}.pdf', format='pdf')
plt.close()
