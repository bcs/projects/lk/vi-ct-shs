import numpy as np
import matplotlib
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import pickle
import scipy.stats as st
import torch
from src.utils.paths import DATA_PATH, CHECKPOINT_PATH, PLOTS_PATH

matplotlib.rcParams['axes.linewidth'] = 1.5  # set the value globally
matplotlib.rcParams['mathtext.fontset'] = 'cm'
matplotlib.rcParams['mathtext.rm'] = 'serif'

# -------------------- Loading --------------------
# Get experiment name (file names have to match)
experiment_name = __file__[__file__.rfind(
        '/') + 1: -3]  # extract the filename between last `/' and the `.npy', i.e., the experiment's name

# Load synthetic data
with open('../../' + DATA_PATH + experiment_name + '/forward.pkl', 'rb') as f:
        trajectory = pickle.load(f)

time_grid = np.array(trajectory['time_grid'])
diffusion_path = np.array(trajectory['sde'])
t_obs = np.array(trajectory['t_obs'])
X_obs = np.array(trajectory['X_obs'])

# Load VSHSSMF object
timestamp = '20211006-130951'
#timestamp = '20210517-153642'
S = torch.load('../../' + CHECKPOINT_PATH + f'vssde_fit_1D_4well_learn_params_no_obscov_eqspaced_obs_largeobs_{timestamp}_iter_final.pt')  # TODO

# -------------------- Plotting --------------------
q_z = S.posterior_mjp(time_grid)
mu = S.posterior_mean(time_grid)
Sigma = S.posterior_cov(time_grid)

# Plot the mode reconstruction (top left), the Y-`marginals' (bottom left) and right next to that the potential shape (bottom right)
widths = [12, 1]
heights = [1, 5]
fig = plt.figure(figsize=(5, 3.5))
gs = gridspec.GridSpec(ncols=2, nrows=2, figure=fig, width_ratios=widths, height_ratios=heights, wspace=0.05, hspace=0.08)
ax1 = fig.add_subplot(gs[0, 0])
ax3 = fig.add_subplot(gs[1, 0], sharex=ax1)
ax4 = fig.add_subplot(gs[1, 1], sharey=ax3)
plt.setp(ax1.get_xticklabels(), visible=False)  # No x-ticklabels on q plot


# 1. q(Z=0) reconstruction
ax1.plot(time_grid, S.posterior_mjp(time_grid)[0], c='blue', lw=2)
ax1.plot(time_grid, S.posterior_mjp(time_grid)[1], c='red', lw=2)
ax1.plot(time_grid, S.posterior_mjp(time_grid)[2], c='green', lw=2)
ax1.plot(time_grid, S.posterior_mjp(time_grid)[3], c='orange', lw=2)
ax1.set_yticks([0, 1])
ax1.set_ylabel('$q_Z$', fontsize='x-large')
ax1.yaxis.set_label_coords(-0.06, 0.5)

x = np.linspace(-1.3, 1.3, 1000)
grid_midpoints = x[:-1] + 0.5 * np.diff(x)
mat = []
for t_i, t in enumerate(time_grid):
    density = np.sum(np.array([q_z[i, t_i] * st.norm(loc=mu[i, 0, t_i], scale=np.sqrt(Sigma[i, 0, 0, t_i])).pdf(grid_midpoints) for i in range(S.n_states)]), axis=0)
    mat.append(density)

mat = np.array(mat)
ax3.matshow(mat.T, cmap='hot', aspect='auto', origin='lower', extent=(0, 20, -1.3, 1.3))
ax3.xaxis.set_ticks_position('bottom')

ax3.plot(time_grid, diffusion_path, alpha=.1, c='white', lw=.3)
ax3.scatter(t_obs, X_obs[0], marker='x', c='white', alpha=.6)
ax3.set_xlabel('Time', fontdict={'size': 'x-large', 'name': 'cmr10'})
ax3.set_ylabel('$Y$', fontsize='x-large')
ax3.yaxis.set_label_coords(-0.06, 0.5)

## Add colored arrows matching q(Z)-coloring -> indicate the modes
#mu_weighted_mean = np.sum(q_z[:, None, :] * mu, axis=2)/np.sum(q_z, axis=1, keepdims=True)
#ax3.annotate('', xy=(0.2, mu_weighted_mean[0]), xytext=(-1.2, mu_weighted_mean[0]),
#             arrowprops={'arrowstyle': '-|>', 'lw': 4, 'color': 'blue'},
#             va='center', transform=ax3.transData)
#ax3.annotate('', xy=(0.2, mu_weighted_mean[1]), xytext=(-1.2, mu_weighted_mean[1]),
#             arrowprops={'arrowstyle': '-|>', 'lw': 4, 'color': 'r'},
#             va='center', transform=ax3.transData)
#ax3.annotate('', xy=(0.2, mu_weighted_mean[2]), xytext=(-1.2, mu_weighted_mean[2]),
#             arrowprops={'arrowstyle': '-|>', 'lw': 4, 'color': 'g'},
#             va='center', transform=ax3.transData)
#ax3.annotate('', xy=(0.2, mu_weighted_mean[3]), xytext=(-1.2, mu_weighted_mean[3]),
#             arrowprops={'arrowstyle': '-|>', 'lw': 4, 'color': 'orange'},
#             va='center', transform=ax3.transData)

# Add colored learned prior means
mu_0 = -1 * np.linalg.inv(S.prior_slope[0].detach().numpy()) @ S.prior_intercept[0].detach().numpy()
mu_1 = -1 * np.linalg.inv(S.prior_slope[1].detach().numpy()) @ S.prior_intercept[1].detach().numpy()
mu_2 = -1 * np.linalg.inv(S.prior_slope[2].detach().numpy()) @ S.prior_intercept[2].detach().numpy()
mu_3 = -1 * np.linalg.inv(S.prior_slope[3].detach().numpy()) @ S.prior_intercept[3].detach().numpy()
ax3.annotate('', xy=(0.2, mu_0), xytext=(-1.2, mu_0),
             arrowprops={'arrowstyle': '-|>', 'lw': 4, 'color': 'blue'},
             va='center', transform=ax3.transData)
ax3.annotate('', xy=(0.2, mu_1), xytext=(-1.2, mu_1),
             arrowprops={'arrowstyle': '-|>', 'lw': 4, 'color': 'red'},
             va='center', transform=ax3.transData)
ax3.annotate('', xy=(0.2, mu_2), xytext=(-1.2, mu_2),
             arrowprops={'arrowstyle': '-|>', 'lw': 4, 'color': 'green'},
             va='center', transform=ax3.transData)
ax3.annotate('', xy=(0.2, mu_3), xytext=(-1.2, mu_3),
             arrowprops={'arrowstyle': '-|>', 'lw': 4, 'color': 'orange'},
             va='center', transform=ax3.transData)

x = np.linspace(-1.2, 1.2, 1000)
def V(x, magnitude=1, scale_quartic=1, scale_squared=1):
    return 4 * (x**8 + 3 * np.exp(-80 * x**2) + 2.5 * np.exp(-80 * (x - 0.5)**2) + 2.5 * np.exp(-80 * (x + 0.5)**2))

base = ax4.transData
rot = matplotlib.transforms.Affine2D().rotate_deg(-90)
ax4.plot(x, V(x, scale_quartic=.4, scale_squared=1.2), c='b', transform=rot + base, lw=2)
ax4.set_xticks([], [])
plt.setp(ax4.get_yticklabels(), visible=False)  # No y-ticklabels on double-well plot


plt.subplots_adjust(left=0.092, right=0.99, top=0.985, bottom=0.13)

plt.savefig('../../' + PLOTS_PATH + experiment_name + f'/overview_{timestamp}.pdf', format='pdf')
plt.close()

