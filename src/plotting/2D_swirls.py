import numpy as np
import matplotlib
from matplotlib import cm
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import pickle
import scipy.stats as st
import torch
from src.utils.paths import DATA_PATH, CHECKPOINT_PATH, PLOTS_PATH

matplotlib.rcParams['axes.linewidth'] = 1.5 # set the value globally
matplotlib.rcParams['mathtext.fontset'] = 'cm'
matplotlib.rcParams['mathtext.rm'] = 'serif'

# -------------------- Loading --------------------
# Get experiment name (file names have to match)
experiment_name = __file__[__file__.rfind(
        '/') + 1: -3]  # extract the filename between last `/' and the `.npy', i.e., the experiment's name

# Load synthetic data
with open('../../' + DATA_PATH + experiment_name + '/forward.pkl', 'rb') as f:
        trajectory = pickle.load(f)

idx = 1
time_grid = trajectory['time_grid']
diffusion_path = trajectory['sde'][idx]
mjp_path = trajectory['mjp'][idx]
X_obs = trajectory['X_obs'][idx]
t_obs = trajectory['t_obs'][idx]

# Load VSHSSMF object
#timestamp = '20210511-095701'
timestamp = '20211005-130724'
S = torch.load('../../' + CHECKPOINT_PATH + f'vssde_fit_2D_swirls_learn_params_no_obscov_more_obs_round_circles_{timestamp}_iter_final.pt')  # TODO


# -------------------- Plotting --------------------
q_z = S.posterior_mjp(time_grid)
mu = S.posterior_mean(time_grid)
Sigma = S.posterior_cov(time_grid)
mu_avg = (q_z[:, None, :] * mu).sum(0)

# Plot three things:
#  1. The ground-truth diffusion trajectory with observations
#  2. The posterior marginals q(z) for all modes z
#  3. The map path (TODO phase or time course?)
#fig, ax = plt.subplots(3, sharex=True, gridspec_kw={'hspace': 0.1, 'height_ratios': [1, 1, 5]}, figsize=(5, 5))
fig = plt.figure(figsize=(10, 3))
gs0 = gridspec.GridSpec(nrows=1, ncols=2, width_ratios=[1, 2])

# 1. Ground-truth trace
gs00 = gridspec.GridSpecFromSubplotSpec(2, 1, subplot_spec=gs0[0])
ax0 = fig.add_subplot(gs00[0])
ax0.plot(time_grid, mjp_path, c='tab:blue', lw=2)
ax0.tick_params(labelbottom=False)
ax0.set_yticks([0, 1])
ax0.set_yticklabels(['1', '2'])
ax0.set_ylabel('$Z$', fontsize='x-large')


# 2. q(z) - reconstruction
ax1 = fig.add_subplot(gs00[1], sharex=ax0)
ax1.plot(time_grid, q_z[0], c='tab:blue', lw=2)
ax1.set_yticks([0, 1])
ax1.set_xlabel('Time', fontdict={'size': 'x-large', 'name':'cmr10'})
ax1.set_ylabel('$q_Z$', fontsize='x-large')

#. Quiver of original dynamics
gs01 = gs0[1].subgridspec(1, 2, hspace=.2)
ax2 = fig.add_subplot(gs01[0])
beta = np.array([[[-0.1, 1.4],
                   [-2.6, 0.6]],
                  [[0.6, -1.4],
                   [2.6, 0.6]]])
alpha = np.array([[5., 0.],
                 [-5., 0.]])

x = np.linspace(-16, 16, 20)
y = np.linspace(-13, 10.5, 20)
X_grid, Y_grid = np.meshgrid(x, y)
U1, V1 = np.zeros(X_grid.shape), np.zeros(X_grid.shape)
U2, V2 = np.zeros(X_grid.shape), np.zeros(X_grid.shape)

for i in range(len(x)):
    for j in range(len(y)):
        res1 = beta[0] @ (alpha[0] - np.array([x[i], y[j]]))
        res2 = beta[1] @ (alpha[1] - np.array([x[i], y[j]]))
        U1[j, i] = res1[0]
        V1[j, i] = res1[1]
        U2[j, i] = res2[0]
        V2[j, i] = res2[1]

ax2.quiver(X_grid, Y_grid, U1, V1, color='blue', alpha=0.6, width=0.010, headwidth=3.0)
ax2.quiver(X_grid, Y_grid, U2, V2, color='red', alpha=0.6, width=0.010, headwidth=3.0)
ax2.set_ylim([-13, 10])

# Plot ground-truth diffusion, colored according to respective q_z state
switching_indices = np.where(np.diff(mjp_path) != 0)[0]
colors = ['blue', 'red']
last_idx = 0
for idx in switching_indices:
    ax2.plot(diffusion_path[0, last_idx:idx], diffusion_path[1, last_idx:idx], alpha=.8, c=colors[int(mjp_path[last_idx+1])])
    last_idx = idx

ax2.plot(diffusion_path[0, last_idx:], diffusion_path[1, last_idx:], alpha=.8,
         c=colors[int(mjp_path[-1])])

ax2.scatter(X_obs[:, 0], X_obs[:, 1], marker='x', c='k', alpha=.5, zorder=0)
ax2.set_xlabel('$Y_1$', fontsize='x-large')
ax2.set_ylabel('$Y_2$', fontsize='x-large')
ax2.yaxis.set_label_coords(-0.1, 0.5)

# Quiver of reconstructed (prior) dynamics and MAP-colored mean
ax3 = fig.add_subplot(gs01[1], sharey=ax2)

beta = S.prior_slope
alpha = S.prior_intercept

X_grid, Y_grid = np.meshgrid(x, y)
U1, V1 = np.zeros(X_grid.shape), np.zeros(X_grid.shape)
U2, V2 = np.zeros(X_grid.shape), np.zeros(X_grid.shape)

for i in range(len(x)):
    for j in range(len(y)):
        res1 = beta[0] @ np.array([x[i], y[j]]) + alpha[0]
        res2 = beta[1] @ np.array([x[i], y[j]]) + alpha[1]
        U1[j, i] = res1[0]
        V1[j, i] = res1[1]
        U2[j, i] = res2[0]
        V2[j, i] = res2[1]

ax3.quiver(X_grid, Y_grid, U1, V1, color='red', alpha=0.6, width=0.010, headwidth=3.0)
ax3.quiver(X_grid, Y_grid, U2, V2, color='blue', alpha=0.6, width=0.010, headwidth=3.0)

#posterior_traj = S.sample('posterior', 10)

# Plot MAP-colored mean path
MAP_indices = []
for t_i, t in enumerate(time_grid):
    st_objects = [st.multivariate_normal(mean=mu[z, :, t_i], cov=Sigma[z, :, :, t_i]) for z in range(S.n_states)]
    density_at_mean = np.array([density.pdf(mu[:, :, t_i]) for density in st_objects])
    weighted_density_at_mean = q_z[:, t_i, None] * density_at_mean

    overall_density_at_mean = np.sum(weighted_density_at_mean, axis=0)
    MAP_indices.append(np.argmax(overall_density_at_mean))

MAP_indices = np.array(MAP_indices)
MAP = np.array([mu[MAP_indices[i], :, i] for i in range(time_grid.shape[0])])

colors = ['red', 'blue']

switching_indices = np.where(np.diff(MAP_indices) != 0)[0]
last_idx = 0
for idx in switching_indices:
    ax3.plot(MAP[last_idx:idx, 0], MAP[last_idx:idx, 1], alpha=.8, c=colors[int(MAP_indices[last_idx+1])])
    last_idx = idx

ax3.plot(MAP[last_idx:, 0], MAP[last_idx:, 1], alpha=.8, c=colors[int(MAP_indices[-1])])

ax3.scatter(X_obs[:, 0], X_obs[:, 1], marker='x', alpha=.5, c='k', zorder=0)
#ax3.plot(posterior_traj.sde[7, 0], posterior_traj.sde[7, 1], alpha=.7, c='gray')

ax3.set_xlabel('$Y_1$', fontsize='x-large')

ax3.tick_params(labelleft=False)
plt.subplots_adjust(left=0.05, right=0.99, top=0.97, bottom=0.165)

plt.savefig('../../' + PLOTS_PATH + experiment_name + f'/overview_{timestamp}.pdf', format='pdf')
plt.close()


# Quiver of reconstructed (posterior) dynamics
ax3 = fig.add_subplot(gs[:, 2], sharey=ax2)

beta = S.variational_slope
alpha = S.variational_intercept

interpolants_time = beta.time_grid
q_z_interpolants = S.posterior_mjp(interpolants_time)

beta_q = np.sum(q_z_interpolants[:, None, None, :] * beta.interpolants, axis=-1)/(np.sum(q_z_interpolants, axis=1)[:, None, None])
alpha_q = np.sum(q_z_interpolants[:, None, :] * alpha.interpolants, axis=-1)/(np.sum(q_z_interpolants, axis=1)[:, None])

x = np.linspace(-11, 11, 15)
y = np.linspace(-8.5, 5.5, 15)
X_grid, Y_grid = np.meshgrid(x, y)
U1, V1 = np.zeros(X_grid.shape), np.zeros(X_grid.shape)
U2, V2 = np.zeros(X_grid.shape), np.zeros(X_grid.shape)

for i in range(len(x)):
    for j in range(len(y)):
        res1 = beta_q[0] @ np.array([x[i], y[i]]) + alpha_q[0]
        res2 = beta_q[1] @ np.array([x[i], y[i]]) + alpha_q[1]
        U1[j, i] = res1[0]
        V1[j, i] = res1[1]
        U2[j, i] = res2[0]
        V2[j, i] = res2[1]

ax3.quiver(X_grid, Y_grid, U1, V1, color='tab:blue', alpha=0.6, width=0.010, headwidth=3.0)
ax3.quiver(X_grid, Y_grid, U2, V2, color='tab:red', alpha=0.6, width=0.010, headwidth=3.0)



MAP_indices = []
for t_i, t in enumerate(time_grid):
    st_objects = [st.multivariate_normal(mean=mu[z, :, t_i], cov=Sigma[z, :, :, t_i]) for z in range(S.n_states)]
    density_at_mean = np.array([density.pdf(mu[:, :, t_i]) for density in st_objects])
    weighted_density_at_mean = q_z[:, t_i, None] * density_at_mean

    overall_density_at_mean = np.sum(weighted_density_at_mean, axis=0)
    MAP_indices.append(np.argmax(overall_density_at_mean))


MAP_indices = np.array(MAP_indices)
MAP = np.array([mu[MAP_indices[i], :, i] for i in range(time_grid.shape[0])])

ax[0].plot(time_grid, diffusion_path[:, 0], alpha=.4, lw=.8)
ax[0].plot(time_grid, diffusion_path[:, 1], alpha=.4, lw=.8)
ax[0].plot(time_grid, MAP[:, 0], c='tab:blue', lw=2.5)
ax[0].plot(time_grid, MAP[:, 1], c='tab:orange', lw=2.5)
ax[0].scatter(t_obs, X_obs[0], alpha=.6, marker='x', c='tab:blue')
ax[0].scatter(t_obs, X_obs[1], alpha=.6, marker='x', c='tab:orange')
ax[0].set_ylabel('State', fontdict={'size': 'x-large', 'name':'cmr10'})
ax[0].yaxis.set_label_coords(-0.04, 0.5)


# 2. q(Z) reconstruction
#ax1 = fig.add_subplot(gs[1, 0], sharex=ax0)
ax[1].plot(time_grid, q_z[0], c='b', lw=2)
ax[1].plot(time_grid, q_z[1], c='r', lw=2)
ax[1].plot(time_grid, q_z[2], c='g', lw=2)
ax[1].set_yticks([0, 1])
ax[1].yaxis.set_label_coords(-0.06, 0.5)
ax[1].set_xlabel('Time', fontdict={'size': 'x-large', 'name': 'cmr10'})
ax[1].set_ylabel('$q(Z)$', fontsize='x-large')
ax[1].yaxis.set_label_coords(-0.04, 0.6)
plt.subplots_adjust(left=0.07, right=0.99, top=0.99, bottom=0.09)

plt.savefig('../../' + PLOTS_PATH + experiment_name + f'/overview_{timestamp}.pdf', format='pdf')
plt.close()


# Plot the potential landscape, the observations and the MAP path as phase plot
def U(x, y):
    """
    Potential as defined in Wu et al.

    Parameter
    ---------
    x, y : float
        2D-position
    """
    return (3 * np.exp(-x**2 - (y - 3/3)**2) - 3 * np.exp(-x**2 - (y - 5/3)**2)
            - 5 * np.exp(-(x-1.5)**2 - (y + 1/3)**2) - 5 * np.exp(-(x+1.5)**2 - (y+1/3)**2) + 1/5 * x**4 + 1/5 * (y - 1/3)**4)

x = np.linspace(-2.5, 2.5, 200)
y = np.linspace(-2, 2.5, 200)
X, Y = np.meshgrid(x, y)

plt.contourf(X, Y, U(X, Y), levels=25, cmap=cm.inferno)
plt.xlabel('x')
plt.ylabel('y')
plt.scatter(X_obs[0], X_obs[1], c='white', alpha=.6, marker='x')
# Plot q-normalized mode-means
mu_weighted_mean = np.sum(q_z[:, None, :] * mu, axis=2)/np.sum(q_z, axis=1, keepdims=True)
plt.scatter(mu_weighted_mean[0, 0], mu_weighted_mean[0, 1], marker='D', s=80, c='b')
plt.scatter(mu_weighted_mean[1, 0], mu_weighted_mean[1, 1], marker='D', s=80, c='r')
plt.scatter(mu_weighted_mean[2, 0], mu_weighted_mean[2, 1], marker='D', s=80, c='g')


#plt.savefig('../plots/energy_landscape.pdf', format='pdf')

