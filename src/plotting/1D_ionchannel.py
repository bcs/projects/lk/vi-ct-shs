import copy

import numpy as np
import matplotlib
import matplotlib.colors as colors
import matplotlib.pyplot as plt
import scipy.stats as st
import torch
from src.utils.paths import DATA_PATH, CHECKPOINT_PATH, PLOTS_PATH

matplotlib.rcParams['axes.linewidth'] = 1.5 # set the value globally
matplotlib.rcParams['mathtext.fontset'] = 'cm'
matplotlib.rcParams['mathtext.rm'] = 'serif'

# -------------------- Loading --------------------
# Get experiment name (file names have to match)
experiment_name = __file__[__file__.rfind(
        '/') + 1: -3]  # extract the filename between last `/' and the `.npy', i.e., the experiment's name

## Load synthetic data
diffusion_path = np.genfromtxt('../../' + DATA_PATH + 'ionchannels/KcvMT325_140mV.asc')
diffusion_path = diffusion_path[9200:14200]
diffusion_path *= 1.6e11
#with open('../../' + DATA_PATH + experiment_name + '/forward.pkl', 'rb') as f:
#        trajectory = pickle.load(f)

#time_grid = trajectory['time_grid']
#diffusion_path = trajectory['sde']
#jump_path = trajectory['mjp']

# Load VSHSSMF object
#timestamp = '20210513-184218'
timestamp = '20211004-130323'
S = torch.load('../../' + CHECKPOINT_PATH + f'vssde_fit_1D_ionchannel_subconductivities_no_obscov_full_trajectory_{timestamp}_iter_final.pt')  # TODO


# -------------------- Plotting --------------------
time_grid = np.linspace(0, 1, 5000)
q_z = S.posterior_mjp(time_grid)
mu = S.posterior_mean(time_grid)
Sigma = S.posterior_cov(time_grid)
mu_avg = (q_z[:, None, :] * mu).sum(0).squeeze(0)
scaling_factor = 1.6e11  # re-scaled data with this factor to have data roughly in range [0, 1]

# Plot three things:
#  1. The posterior marginal q(Z=0) (which hopefully resembles the ground-truth MJP trajectory)
#  2. The posterior means mu(z, t) with their 1-sigma-tubes with some posterior diffusion trajectories
fig, ax = plt.subplots(3, sharex=True, gridspec_kw={'hspace': 0.1, 'height_ratios': [1, 1, 4]}, figsize=(5, 3.5))

# 1. q(Z=0) reconstruction
ax[0].plot(time_grid, q_z[0], c='blue', lw=2)
ax[0].plot(time_grid, q_z[1], c='red', lw=2)
ax[0].plot(time_grid, q_z[2], c='green', lw=2)
ax[0].set_ylabel('$q_Z$', fontsize='x-large')
ax[0].set_yticks([0, 1])
ax[0].yaxis.set_label_coords(-0.06, 0.5)

# 2. MAP path
MAP = np.argmax(q_z, axis=0)
tmp = copy.deepcopy(MAP)
MAP[tmp == 0] = 2
MAP[tmp == 1] = 0
MAP[tmp == 2] = 1

ax[1].plot(time_grid, MAP, lw=2, c='tab:blue')
ax[1].set_ylabel('$Z^{\mathrm{MAP}}$', fontsize='x-large')
ax[1].set_yticks([0, 1, 2])
ax[1].set_yticklabels([1, 2, 3])
ax[1].annotate('', xy=(0.99, 2), xytext=(1.06, 2),
             arrowprops={'arrowstyle': '-|>', 'lw': 3, 'color': 'blue'},
             va='center', transform=ax[1].transData)
ax[1].annotate('', xy=(0.99, 0), xytext=(1.06, 0),
             arrowprops={'arrowstyle': '-|>', 'lw': 3, 'color': 'red'},
             va='center', transform=ax[1].transData)
ax[1].annotate('', xy=(0.99, 1), xytext=(1.06, 1),
             arrowprops={'arrowstyle': '-|>', 'lw': 3, 'color': 'green'},
             va='center', transform=ax[1].transData)
ax[1].set_yticklabels([])
ax[1].yaxis.set_label_coords(-0.06, 0.5)


# 3. Diffusion plot
x = np.linspace(-2e-12, 8e-12, 1000)
grid_midpoints = x[:-1] + 0.5 * np.diff(x)
mat = []
for t_i, t in enumerate(time_grid):
    mat.append(q_z[0, t_i] * st.norm(loc=mu[0, 0, t_i]/scaling_factor, scale=np.sqrt(Sigma[0, 0, 0, t_i])/scaling_factor).pdf(grid_midpoints)
               + q_z[1, t_i] * st.norm(loc=mu[1, 0, t_i]/scaling_factor, scale=np.sqrt(Sigma[1, 0, 0, t_i])/scaling_factor).pdf(grid_midpoints)
               + q_z[2, t_i] * st.norm(loc=mu[2, 0, t_i]/scaling_factor, scale=np.sqrt(Sigma[2, 0, 0, t_i])/scaling_factor).pdf(grid_midpoints))
mat = np.array(mat)
ax[2].matshow(mat.T, cmap='hot', aspect='auto', origin='lower', extent=(0, 1, -2e-12, 8e-12))
ax[2].xaxis.set_ticks_position('bottom')

ax[2].plot(time_grid, diffusion_path/scaling_factor, lw=.5, alpha=.3, c='white')

# Add colored arrows matching q(Z)-coloring -> indicate the modes
mu_weighted_mean = np.sum(q_z[:, None, :] * mu, axis=2)/np.sum(q_z, axis=1, keepdims=True)
ax[2].annotate('', xy=(0.99, mu_weighted_mean[0]/scaling_factor), xytext=(1.06, mu_weighted_mean[0]/scaling_factor),
             arrowprops={'arrowstyle': '-|>', 'lw': 3, 'color': 'blue'},
             va='center', transform=ax[1].transData)
ax[2].annotate('', xy=(0.99, mu_weighted_mean[1]/scaling_factor), xytext=(1.06, mu_weighted_mean[1]/scaling_factor),
             arrowprops={'arrowstyle': '-|>', 'lw': 3, 'color': 'red'},
             va='center', transform=ax[1].transData)
ax[2].annotate('', xy=(0.99, mu_weighted_mean[2]/scaling_factor), xytext=(1.06, mu_weighted_mean[2]/scaling_factor),
             arrowprops={'arrowstyle': '-|>', 'lw': 3, 'color': 'green'},
             va='center', transform=ax[1].transData)


ax[2].set_xlabel('Time [s]', fontdict={'size': 'x-large', 'name':'cmr10'})
ax[2].set_ylabel('Current [pA]', fontdict={'size': 'x-large', 'name':'cmr10'})
ax[2].set_xticks([0, 0.5, 1])
ax[2].set_xticklabels([0, 0.5, 1])
ax[2].set_ylim([-1e-12, 7e-12])
ax[2].set_yticks(np.array([0, 2, 4, 6]) * 1e-12)
ax[2].set_yticklabels([0, 2, 4, 6])
ax[2].yaxis.set_label_coords(-0.06, 0.5)

plt.subplots_adjust(left=0.10, right=0.94, top=0.98, bottom=0.14)
plt.savefig('../../' + PLOTS_PATH + experiment_name + f'/overview_{timestamp}.pdf', format='pdf')
plt.close()

fig, ax = plt.subplots(1)
ax.plot(time_grid, diffusion_path[0], c='b')
for i in range(n_trajectories):
        ax.plot(trajectory_posterior.time_grid, trajectory_posterior.sde[i, 0], color='dimgray', alpha=.4, lw=.7)

ax.set_xlabel('Time', fontdict={'size': 'x-large', 'name':'cmr10'})
ax.set_ylabel('$Y$', fontsize='x-large')
ax.set_ylim([-2, 2.5])
ax.set_yticks([-2, -1, 0, 1, 2])
ax.yaxis.set_label_coords(-0.05, 0.5)

plt.subplots_adjust(left=0.08, right=0.99, top=0.99, bottom=0.09)

plt.savefig('../../' + PLOTS_PATH + experiment_name + f'/posterior_samples_{timestamp}.pdf', format='pdf')
plt.close()
