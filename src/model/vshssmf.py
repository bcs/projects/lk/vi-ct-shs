import matplotlib.pyplot as plt
import numpy as np
import scipy.integrate as integrate
import torch
import torch.nn as nn
from joblib import Parallel, delayed
from src.utils.utils import cat_ode_solutions, get_constant_function, ReshapedOdeSolution, discrete_kl, gaussian_kl, \
    quad_form, quadrature_intervals, quad_vec_intervals
from copy import deepcopy
from torch.optim import Optimizer, LBFGS
from typing import Any, Iterable, Tuple, Union, Callable, List, Dict
import logging
from src.utils.paths import CHECKPOINT_PATH
from datetime import datetime
from scipy import interpolate as interpolate

EPS = 1e-6


class VSHSSMF(nn.Module):
    def __init__(self, t_span: List[Any], t_obs: torch.Tensor = None, obs_data: torch.Tensor = None,
                 n_states: int = None, Optimizer_constructor: Optimizer = None, optimizer_opts: Dict = None,
                 obs_cov: torch.Tensor = None, prc_cov: torch.Tensor = None, initial_prior_mean: torch.Tensor = None,
                 initial_prior_cov: torch.Tensor = None, prior_slope: torch.Tensor = None,
                 prior_intercept: torch.Tensor = None, initial_prior_mjp: torch.Tensor = None,
                 prior_rate_mtx: torch.Tensor = None, initial_posterior_mjp: torch.Tensor = None,
                 posterior_mjp: ReshapedOdeSolution = None, mjp_costate: ReshapedOdeSolution = None,
                 initial_posterior_mean: torch.Tensor = None, initial_posterior_cov: torch.Tensor = None,
                 posterior_mean: ReshapedOdeSolution = None, posterior_cov: ReshapedOdeSolution = None,
                 mean_costate: ReshapedOdeSolution = None, cov_costate: ReshapedOdeSolution = None,
                 verbose: bool = False, rate_clip=100., slope_clip=-1 * EPS):
        """
        Class implementing the VSHSSMF framework.

        :param rate_clip: TODO
        :param slope_clip: TODO
        :param t_span: [2,] iterable,  the total time interval
        :param t_obs: [N,] tensor, observation time points
        :param obs_data: [n_dim, N] tensor, obs_data[:, i] holds observation vector obtained at t_obs[i]
        :param n_states: int number of latent MJP states (Only used if priors not given, otherwise inferred)
        :param Optimizer_constructor Optimizer Constructor --> instance is used for the gradient based algorithms (default LBFGS with linesearch)
        :param optimizer_opts Dict containing options passed to Optimizer_constructor
        :param obs_cov: [n_dim, n_dim] tensor, the covariance matrix of the observation process
        :param prc_cov: [n_states, n_dim, n_dim] tensor, covariance matrix of the latent diffusion process
        :param initial_prior_mean: [n_states, n_dim] tensor, the initial mean of the prior diffusion process
        :param initial_prior_cov: [n_states, n_dim, n_dim] tensor, the initial covariance of the prior diffusion process
        :param prior_slope: [n_states,n_dim, n_dim] tensor, prior slope
        :param prior_intercept: [n_states,n_dim] tensor, prior intercept
        :param initial_prior_mjp: [n_states] tensor, the MJP inital distribution of the prior
        :param prior_rate_mtx: [n_states, n_states] tensor, the MJP rate matrix
        :param initial_posterior_mjp: [n_states]  tensor, the MJP inital distribution of the posterior (start value)
        :param posterior_mjp: (t)[n_states]  ReshapedODESolution, the marginal posterior MJP for all time points t (start value)
        :param mjp_costate: (t)[n_states]  ReshapedODESolution, the marginal costate MJP for all time points t (start value)
        :param initial_posterior_mean: [n_states,n_dim]  tensor, the diffusion mean initial value of the posterior (start value)
        :param initial_posterior_cov: [n_states,n_dim,n_dim]  tensor, the diffusion covariance initial value of the posterior (start value)
        :param posterior_mean: (t)[n_states,n_dim]  ReshapedODESolution, the mean of the variational diffusion for all time points t (start value)
        :param posterior_cov: (t)[n_states,n_dim,n_dim]  ReshapedODESolution, the covariance of the variational diffusion for all time points t (start value)
        :param mean_costate: (t)[n_states,n_dim]  ReshapedODESolution, the mean costate of the variational diffusion for all time points t (start value)
        :param cov_costate: (t)[n_states,n_dim,n_dim]  ReshapedODESolution, the covariance costate of the variational diffusion for all time points t (start value)
        :param verbose: boolean flag for logging
        :param constraint_rate: float, an upper bound on the variational rates
        :param constraint_slope: float, an upper bound on the entry-wise absolute value of the slope matrices
        :param constraint_slope: float, an upper bound on the entry-wise absolute value of the intercept vectors
        """

        if verbose:
            logging.basicConfig(level=logging.DEBUG)
        else:
            logging.basicConfig(level=logging.INFO)

        super().__init__()

        # --------------------------------------------
        # ------------ Create properties ------------
        # --------------------------------------------

        # 1) ------------ Model properties ------------
        self._t_span = None
        self._n_dim = None
        self._n_states = n_states

        # 2) ------------ Observation data properties ------------
        self._interval_boundaries = None
        self._n_intervals = None
        self.register_buffer('_t_obs', None)
        self.register_buffer('_obs_data', None)

        # 3) ------------ Observation model properties ------------
        self.register_parameter('_obs_cov_chol', None)
        self.register_buffer('_obs_cov_logdet', None)
        self.register_buffer('_obs_cov_inv', None)

        # 4) ------------ Prior diffusion model properties ------------
        self.register_parameter('_prc_cov_chol', None)
        self.register_buffer('_prc_cov_inv', None)
        self.register_buffer('_initial_prior_mean', None)
        self.register_buffer('_initial_prior_cov', None)
        self.register_buffer('_prior_slope', None)
        self.register_buffer('_prior_intercept', None)
        self.register_buffer('_expected_drift_mismatch_outer', None)
        self.register_buffer('_cov_costate_integrated', None)

        # 5) ------------ Prior mjp model properties ------------
        self.register_buffer('_initial_prior_mjp', None)
        self.register_buffer('_prior_rate_mtx', None)
        self.register_buffer('_prior_rate_mtx_zerodiag', None)

        # 6) ------------ Posterior mjp model properties ------------
        self.register_parameter('_initial_posterior_mjp_params', None)  # optimizable parameters
        self.register_buffer('_expected_posterior_rates', None)
        self.register_buffer('_expected_posterior_sojourn_times', None)

        # 7) ------------ Posterior diffusion model properties ------------
        self.register_parameter('_initial_posterior_mean', None)
        self.register_parameter('_initial_posterior_cov_chol', None)  # optimizable parameters

        # --------------------------------------------
        # ----------- Store input values -------------
        # --------------------------------------------
        if obs_data is not None:
            self._n_dim = obs_data.shape[0]
        elif prior_intercept is not None:
            self._n_dim = prior_intercept.shape[1]
        else:
            raise AttributeError

        # 1) ------------ Model properties ------------
        self.t_span = t_span

        # 2) ------------ Observation data properties ------------
        if t_obs is not None:
            self.t_obs = t_obs
            self.obs_data = obs_data

        # 3) ------------ Observation model properties ------------
        if obs_cov is None:
            obs_cov = torch.eye(self.n_dim)
        self.obs_cov = obs_cov

        # 4) ------------ Prior diffusion model properties ------------
        if prc_cov is None:
            prc_cov = torch.eye(self.n_dim)[None, ...].repeat(self.n_states, 1, 1)
        self.prc_cov = prc_cov

        if initial_prior_mean is None:
            initial_prior_mean = torch.zeros(self.n_states, self.n_dim)
        self.initial_prior_mean = initial_prior_mean

        if initial_prior_cov is None:
            initial_prior_cov = torch.eye(self.n_dim)[None, ...].repeat(self.n_states, 1, 1)
        self.initial_prior_cov = initial_prior_cov

        if prior_slope is None:
            prior_slope = -torch.ones(self.n_states, self.n_dim, self.n_dim)
        self.prior_slope = prior_slope

        if prior_intercept is None:
            prior_intercept = -torch.ones(self.n_states, self.n_dim)
        self.prior_intercept = prior_intercept

        # 5) ------------ Prior mjp model properties ------------
        if initial_prior_mjp is None:
            initial_prior_mjp = torch.ones(self.n_states) / self.n_states
        self.initial_prior_mjp = initial_prior_mjp

        if prior_rate_mtx is None:
            prior_rate_mtx = torch.ones(self.n_states, self.n_states, dtype=torch.float64) - self.n_states * torch.eye(self.n_states, dtype=torch.float64)
        self.prior_rate_mtx = prior_rate_mtx

        # 6) ------------ Posterior mjp model properties ------------
        if initial_posterior_mjp is None:
            initial_posterior_mjp = torch.ones(self.n_states, dtype=torch.float64) / self.n_states  # high precision required
        self.initial_posterior_mjp = torch.as_tensor(initial_posterior_mjp, dtype=torch.float64)

        if posterior_mjp is None:
            posterior_mjp = get_constant_function(t_span, 1 / self.n_states, shape=(self.n_states))
        self.posterior_mjp = posterior_mjp

        if mjp_costate is None:
            mjp_costate = get_constant_function(t_span, 0.0, shape=(self.n_states))
        self.mjp_costate = mjp_costate

        # 7) ------------ Posterior diffusion model properties ------------
        if initial_posterior_mean is None:
            initial_posterior_mean = torch.zeros((self.n_states, self.n_dim))
        self.initial_posterior_mean = initial_posterior_mean

        if initial_posterior_cov is None:
            initial_posterior_cov = torch.eye(self.n_dim)[None, ...].repeat(self.n_states, 1, 1)
        self.initial_posterior_cov = initial_posterior_cov

        if posterior_mean is None:
            posterior_mean = get_constant_function(t_span, 0.0, shape=(self.n_states, self.n_dim))
        self.posterior_mean = posterior_mean

        if posterior_cov is None:
            posterior_cov = get_constant_function(t_span, self.prc_cov.detach().numpy().flatten(),
                                                  shape=tuple(self.prc_cov.shape))
        self.posterior_cov = posterior_cov

        if mean_costate is None:
            mean_costate = get_constant_function(t_span, 0.0, shape=(self.n_states, self.n_dim))
        self.mean_costate = mean_costate

        if cov_costate is None:
            cov_costate = get_constant_function(t_span, 0.0, shape=tuple(self.prc_cov.shape))
        self.cov_costate = cov_costate

        # 8) ------------ Constraints on controls -------------------
        self.rate_clip = rate_clip
        self.slope_clip = slope_clip

        if t_obs is not None:
            self.variational_slope = VariationalSlope(prior_slope=self.prior_slope.numpy(),
                                                      t_span=self.t_span, t_obs=self.t_obs.numpy(),
                                                      n_interpolants=10000)

            self.variational_intercept = VariationalIntercept(prior_intercept=self.prior_intercept.numpy(),
                                                              t_span=self.t_span, t_obs=self.t_obs.numpy(),
                                                              n_interpolants=10000)

            self.variational_log_rate_mtx = VariationalLogRateMtx(
                prior_rate_mtx_zerodiag=self.prior_rate_mtx_zerodiag.numpy(),
                t_span=self.t_span, t_obs=self.t_obs.numpy(), n_interpolants=10000)

        # --------------------------------------------
        # ---------------- Other  --------------------
        # --------------------------------------------
        if Optimizer_constructor is None:
            Optimizer_constructor = LBFGS
            if optimizer_opts is None:
                # optimizer_opts = dict(line_search_fn='strong_wolfe', lr=1e-4)
                optimizer_opts = dict(lr=1e-3)

        if optimizer_opts is None:
            optimizer_opts = dict()

        self.optimizer = Optimizer_constructor(self.parameters(), **optimizer_opts)

        self.elbo_history = []  # Store the elbo over iterations

    # --------------------------------------------
    # ------------ Setter and Getter ------------
    # --------------------------------------------
    def variational_rate_mtx(self, t: Union[float, np.ndarray]):
        t = np.asarray(t)

        if t.ndim == 0:
            variational_rate_mtx = np.exp(self.variational_log_rate_mtx(t))  # Dimension ZxZ
            # Set diagonal
            np.einsum('jj->j', variational_rate_mtx)[:] = 0.0
            np.einsum('jj->j', variational_rate_mtx)[:] = -np.sum(variational_rate_mtx, axis=1)

        else:
            variational_rate_mtx = np.exp(self.variational_log_rate_mtx(t))  # Dimension ZxZxT
            np.einsum('jji->ji', variational_rate_mtx)[:] = 0.0  # Zero Sum
            np.einsum('jji->ji', variational_rate_mtx)[:] = -np.sum(variational_rate_mtx, axis=1)

        return variational_rate_mtx

    # 1) ------------ Model setter and getter ------------
    @property
    def t_span(self):
        return self._t_span

    @t_span.setter
    def t_span(self, val):
        assert len(val) == 2, 'Wrong input format for t_span.'
        self._t_span = val

    @property
    def n_dim(self):
        return self._n_dim

    @property
    def n_states(self):
        return self._n_states

    # 2) ------------ Observation data setter and getter ------------
    @property
    def t_obs(self):
        return self._t_obs

    @t_obs.setter
    def t_obs(self, val):
        assert len(
            set(val).intersection(set(self.t_span))) == 0, 'Observation(s) on boundaries. Catch this special case.'
        val = torch.as_tensor(val, dtype=torch.float64)
        self._t_obs = val
        self._n_intervals = len(val) + 1
        self._interval_boundaries = [self.t_span[0]] + val.tolist() + [self.t_span[1]]

    @property
    def interval_boundaries(self):
        return self._interval_boundaries

    @property
    def n_intervals(self):
        return self._n_intervals

    @property
    def obs_data(self):
        return self._obs_data

    @obs_data.setter
    def obs_data(self, val):
        assert val.ndim == 2, 'Wrong input format for x.'
        assert val.shape[1] == len(self.t_obs), '1th dimension of x and length of t_obs must match.'
        val = torch.as_tensor(val, dtype=torch.float)

        self._obs_data = val
        self._n_dim = val.shape[0]

    # 3) ------------ Observation model setter and getter ------------

    @property
    def obs_cov(self):
        L = torch.tril(self.obs_cov_chol)
        L_T = L.transpose(-1, -2)
        return L @ L_T

    @property
    def obs_cov_chol(self):
        return self._obs_cov_chol

    @obs_cov_chol.setter
    def obs_cov_chol(self, val):
        if self._obs_cov_chol is None:
            self._obs_cov_chol = nn.Parameter(val)
        else:
            self._obs_cov_chol.data = val
        self._obs_cov_inv = torch.inverse(self._obs_cov_chol @ self._obs_cov_chol.transpose(-1, -2))
        self._obs_cov_logdet = torch.logdet(self._obs_cov_chol @ self._obs_cov_chol.transpose(-1, -2))

    @obs_cov.setter
    def obs_cov(self, val):
        assert val.ndim == 2, 'Required input format for obs_cov is [n_dim, n_dim]'
        L = torch.cholesky(val)
        self.obs_cov_chol = L

    @property
    def obs_cov_inv(self):
        return self._obs_cov_inv

    @property
    def obs_cov_logdet(self):
        return self._obs_cov_logdet

    # 4) ------------ Prior diffusion model setter and getter ------------
    @property
    def initial_prior_mean(self):
        return self._initial_prior_mean

    @initial_prior_mean.setter
    def initial_prior_mean(self, val):
        self._initial_prior_mean = val

    @property
    def initial_prior_cov(self):
        return self._initial_prior_cov

    @initial_prior_cov.setter
    def initial_prior_cov(self, val):
        self._initial_prior_cov = val

    @property
    def prior_slope(self):
        return self._prior_slope

    @prior_slope.setter
    def prior_slope(self, val):
        assert val.ndim == 3, 'Required input format for prior_slope is [n_states, n_dim, n_dim]'
        assert val.shape[1] == val.shape[2], 'prior_slope must be symmetric'
        val = torch.as_tensor(val, dtype=torch.float)
        self._prior_slope = val

    @property
    def prior_intercept(self):
        return self._prior_intercept

    @prior_intercept.setter
    def prior_intercept(self, val):
        assert val.ndim == 2, 'Wrong input format for prior_intercept'
        val = torch.as_tensor(val, dtype=torch.float)
        self._prior_intercept = val

    @property
    def prc_cov(self):
        L = torch.tril(self.prc_cov_chol)
        L_T = L.transpose(-1, -2)
        return L @ L_T

    @property
    def prc_cov_inv(self):
        return self._prc_cov_inv

    @property
    def prc_cov_chol(self):
        return self._prc_cov_chol

    @prc_cov_chol.setter
    def prc_cov_chol(self, val):
        if self._prc_cov_chol is None:
            self._prc_cov_chol = nn.Parameter(val)
        else:
            self._prc_cov_chol.data = val
        self._prc_cov_inv = torch.inverse(self._prc_cov_chol @ self._prc_cov_chol.transpose(-1, -2))

    @prc_cov.setter
    def prc_cov(self, val):
        assert val.ndim == 3, 'Required input format for prc_cov is [n_states, n_dim, n_dim]'
        L = torch.cholesky(val)
        self.prc_cov_chol = L
        self._n_states = val.shape[0]

    # 5) ------------ Prior mjp model setter and getter ------------
    @property
    def initial_prior_mjp(self):
        return self._initial_prior_mjp

    @initial_prior_mjp.setter
    def initial_prior_mjp(self, val):
        self._initial_prior_mjp = val

    @property
    def prior_rate_mtx(self):
        return self._prior_rate_mtx

    @property
    def prior_rate_mtx_zerodiag(self):
        return self._prior_rate_mtx_zerodiag

    @prior_rate_mtx.setter
    def prior_rate_mtx(self, val):
        val = torch.as_tensor(val, dtype=torch.float64)
        assert val.ndim == 2, 'Required input format for Q matrix is [n_states, n_states]'
        assert val.shape[0] == val.shape[1], 'Q matrix must be symmetric'
        assert torch.isclose(val.sum(1), torch.zeros(val.shape[0], dtype=torch.float64)).all(), 'Q matrix has fulfil row-sum-zero'
        assert val.shape[0] == self.n_states, 'Number of Z-states inconsistent between jump and diffusion processes'
        self._prior_rate_mtx = val
        self._prior_rate_mtx_zerodiag = val - torch.diag(torch.diag(val))

    # 6) ------------ Posterior mjp model properties ------------
    @property
    def initial_posterior_mjp(self):
        return self.log_initial_posterior_mjp.exp()

    @property
    def log_initial_posterior_mjp(self):
        return torch.cat([self._initial_posterior_mjp_params,
                          torch.log1p(-torch.clamp(self._initial_posterior_mjp_params.exp().sum(0, keepdim=True), EPS, 1.-EPS))])

    @initial_posterior_mjp.setter
    def initial_posterior_mjp(self, val):
        self._initial_posterior_mjp_params = nn.Parameter(val.log()[:-1])

    # 7) ------------ Posterior diffusion model properties ------------
    @property
    def initial_posterior_mean(self):
        return self._initial_posterior_mean

    @initial_posterior_mean.setter
    def initial_posterior_mean(self, val):
        if self._initial_posterior_mean is None:
            self._initial_posterior_mean = nn.Parameter(val)
        else:
            self._initial_posterior_mean.data = val

    @property
    def initial_posterior_cov(self):
        L = torch.tril(self._initial_posterior_cov_chol)
        L_T = L.transpose(-1, -2)
        return L @ L_T

    @initial_posterior_cov.setter
    def initial_posterior_cov(self, val):
        L = torch.cholesky(val)
        if self._initial_posterior_cov_chol is None:
            self._initial_posterior_cov_chol = nn.Parameter(L)
        else:
            self._initial_posterior_cov_chol.data = L

    @property
    def initial_posterior_cov_chol(self):
        return self._initial_posterior_cov_chol

    @initial_posterior_cov_chol.setter
    def initial_posterior_cov_chol(self, val):
        if self._initial_posterior_cov_chol is None:
            self._initial_posterior_cov_chol = nn.Parameter(val)
        else:
            self._initial_posterior_cov_chol.data = val

    @property
    def initial_posterior_cov_logdet(self):
        # Sigma_q_logdet=L_q L_q^T -> L_q is lower triangular therefore
        # log det(Sigma)  =log det(L L^T)  =  log det(L)^2=2 log abs( det(L)) = 2*sum log(abs(diag(L)))
        return 2 * self._initial_posterior_cov_chol.diagonal(dim1=-2, dim2=-1).abs().log().sum(dim=-1)

    @property
    def expected_drift_mismatch_outer(self):
        return self._expected_drift_mismatch_outer

    @expected_drift_mismatch_outer.setter
    def expected_drift_mismatch_outer(self, val):
        self._expected_drift_mismatch_outer = val

    @property
    def cov_costate_integrated(self):
        return self._cov_costate_integrated

    @cov_costate_integrated.setter
    def cov_costate_integrated(self, val):
        self._cov_costate_integrated = val

    def fit(self, update_priors: bool = True, max_iter: int = 100, epsrel: float = 1e-6,
            opts_update_prior_params: Dict = None,
            opts_update_posterior_initial_conditions: Dict = None,
            opts_forward_backward_sweep: Dict = None, checkpoint_interval: int = -1,
            checkpoint_name: str = '') -> torch.Tensor:
        """
        Performs a variational (EM) algorithm on the model --> Fits posterior distribution and prior distribution (if flag is set)

        :param max_iter: maximum number of iteration steps for coordinate wise update
        :param update_priors: boolean if prior parameters should be updated using Variational EM
        :param epsrel: convergence criteria --> relative tolerance in ELBO change
        :param opts_update_prior_params: Dict passed to update_prior_params --> see update_prior_params
        :param opts_forward_backward_sweep: Dict passed to opts_forward_backward_sweep --> see opts_forward_backward_sweep
        :param checkpoint_interval: each checkpoint_interval iteration the vssde object is save to the checkpoint path (off if negative)
        :param checkpoint_name: name appended to the checkpoint file

        :return Returns torch Tensor containing ELBO values for the iteration steps
        """
        # Timestamp for checkpoints
        timestamp = datetime.now().strftime('%Y%m%d-%H%M%S')

        # Set default convergence criteria
        if opts_update_prior_params is None:
            opts_update_prior_params = dict(max_steps=10, max_iter=20, decay=0.5, epsrel_int=1e-2, epsrel_elbo=1e-3,
                                            update_obs_cov=True, obs_cov_closed_form=True, update_dispersion=True,
                                            mode_specific_dispersion=False)

        if opts_update_posterior_initial_conditions is None:
            opts_update_posterior_initial_conditions = dict(max_iter=20, epsrel=1e-3)

        if opts_forward_backward_sweep is None:
            opts_forward_backward_sweep = dict(max_iter=20, epsrel=1e-2, rtol=1e-3, max_steps_grad=10, max_iter_grad=10,
                                               decay=0.5)

        logging.info('Starting variational algorithm')
        # Do initial fit
        elbo = self.forward_backward_sweep(**opts_forward_backward_sweep)
        self.elbo_history.append(elbo.clone())
        # Do Posterior and Prior updates until convergence
        for iter in range(max_iter):
            elbo_old = elbo.clone()

            if update_priors:
                self.update_prior_params(**opts_update_prior_params)

            elbo = self.forward_backward_sweep(**opts_forward_backward_sweep)

            self.elbo_history.append(elbo.clone())
            logging.info('Variational algorithm - iteration {} - ELBO {}'.format(iter, elbo))

            # Save checkpoints
            if checkpoint_interval > 0 and (iter % checkpoint_interval == 0 or iter == max_iter - 1):
                torch.save(self,
                           '../' + CHECKPOINT_PATH + 'vssde_fit_{}_{}_iter_{}.pt'.format(checkpoint_name, timestamp,
                                                                                            iter))

            # Check for convergence
            if (elbo - elbo_old).abs() / elbo_old.abs() < epsrel:
                break

        # Save final fit
        if checkpoint_interval > 0:
            torch.save(self,
                       '../' + CHECKPOINT_PATH + 'vssde_fit_{}_{}_iter_final.pt'.format(checkpoint_name, timestamp))
        logging.info('Variational algorithm converged')
        return torch.stack(self.elbo_history)

    # --------------------------------------------
    # ----- 1) Posterior updates -------
    # --------------------------------------------
    def forward_backward_sweep(self, max_iter: int, epsrel: float, rtol: float, max_steps_grad: int, max_iter_grad: int,
                               decay: float) -> torch.Tensor:
        """
        Updates the posterior parameters (diffusion + MJP) using a forward backward sweep until convergence

        :param max_iter: maximum number of iterations of forward backward sweeps
        :param epsrel: convergence criterion -> min relative tolerance in ELBO change
        :param rtol: Relative tolerance for accuracy in ELBO computation

        :return: torch.Tensor containing the final ELBO value
        """
        logging.debug('Starting forward backward sweep')
        elbo = torch.tensor(-float('inf'))
        for iter in range(max_iter):
            elbo_old = elbo.clone()
            # 1) Fit variational diffusion process
            self.update_diffusion_costate()
            self.update_mjp_costate()

            if iter == 0:
                self.update_posterior()  # This needs to be run once before updating the parameters to get a sensible ELBO

            # 2)b) Update variational parameters
            self.update_variational_params(max_steps=max_steps_grad, max_iter=max_iter_grad,
                                           epsrel=epsrel,
                                           decay=decay)  # Note: update_posterior() is called within this function

            # 2)a) Update initial values
            self.update_posterior_initial_conditions(max_steps=max_steps_grad, max_iter=max_iter_grad, epsrel=epsrel,
                                                     decay=decay)
            self.update_posterior()

            # 3) Check for convergence
            elbo = self._elbo(rtol=rtol)
            logging.debug('Forward backward sweep - iteration {} - ELBO {}'.format(iter, elbo))

            if (elbo - elbo_old).abs() / elbo_old.abs() < epsrel:
                logging.debug('Forward backward sweep converged')
                break

        if iter == max_iter - 1:
            logging.debug('Forward backward sweep reached max_iter')
        return elbo

    # --------------------------------------------
    # ----- 1.1) Posterior Costate updates -------
    # --------------------------------------------
    def update_diffusion_costate(self):
        """
        Solve backward ODEs for diffusion costates, i.e. the Lagrange multipliers \lambda and \psi pertaining
        to the \mu- and \Sigma-constraints.
        """

        # Initial Costates are zero and have the dimensionalities correspdoning to mean and covariance
        initial_mean_costate = np.zeros(self.n_states * self.n_dim)
        initial_cov_costate = np.zeros(self.n_states * self.n_dim ** 2)

        # Create piecewise ode solutions between data points
        mean_costate_list = []
        cov_costate_list = []

        # Solve ODEs backward in time
        for interval in reversed(range(self.n_intervals)):
            t_span = self.interval_boundaries[interval + 1], self.interval_boundaries[interval]

            cur_mean_costate = integrate.solve_ivp(self._ode_lambda, t_span, initial_mean_costate,
                                                   method='BDF', dense_output=True, vectorized=True)
            cur_cov_costate = integrate.solve_ivp(self._ode_psi, t_span, initial_cov_costate,
                                                  method='BDF', dense_output=True, vectorized=True)

            mean_costate_list.append(cur_mean_costate.sol)
            cov_costate_list.append(cur_cov_costate.sol)

            # Reset Conditions for costates, cf. appendix_notes Sect. 2.1
            if interval > 0:
                # Set variables

                # observation
                t_i = self.t_obs[interval - 1]
                x_i = self.obs_data[:, interval - 1].numpy()[None, :, None]  # Dimension ZxN'xN

                # variational parameters
                q_z = self.posterior_mjp(t_i)[:, None, None]  # Dimension ZxNxN'
                lambda_ = cur_mean_costate.sol(t_i)  # Dimension flat Z*N
                Sigma_obs_inv = self.obs_cov_inv.detach().numpy()[None, ...]  # Dimension ZxNxN'
                mu = self.posterior_mean(t_i)[..., None]  # Dimension ZxN'xN
                Psi = cur_cov_costate.sol(t_i)  # Dimension flat Z*Nx*N'

                # Reset
                initial_mean_costate = lambda_ - (q_z * Sigma_obs_inv @ (x_i - mu)).flatten()
                initial_cov_costate = Psi + 0.5 * (q_z * Sigma_obs_inv).flatten()

        # Concatenate Piecewise Solutions
        self.mean_costate = cat_ode_solutions(mean_costate_list, shape=(self.n_states, self.n_dim))
        self.cov_costate = cat_ode_solutions(cov_costate_list, shape=(self.n_states, self.n_dim, self.n_dim))

    def _ode_lambda(self, t: float, lambda_: np.ndarray) -> np.ndarray:
        """
        Computes r.h.s. of the ODE for the costate/lagrange multiplier for the posterior mean dynamics

        (Note: input and output is flattend over ZxN dimensions
        + batchwise vectorization over lambda_ is implemented)

        :param t: Evaluation time point
        :param lambda_: [n_states*n_dim] or [n_states*n_dim x num_batches] Current mean costate
        :return: r.h.s. of ODE
        """
        if lambda_.ndim == 2:
            is_batch = True
            num_batches = lambda_.shape[1]
        else:
            is_batch = False
            num_batches = 1

        # Reshape variable to the original space ZxN
        lambda_ = lambda_.T.reshape((num_batches, self.n_states, self.n_dim))[..., None]  # Dimension BxZxN'xN

        # Set variables
        A_q = self.variational_slope(t)[None, ...]  # Dimension BxZxNxN'
        A_q_T = np.moveaxis(A_q, -2, -1)  # Dimension BxZxN'xN
        A_p = self.prior_slope.numpy()[None, ...]  # Dimension BxZxNxN'
        A_bar = A_q - A_p  # Dimension BxZxNxN'

        b_q = self.variational_intercept(t)[None, ..., None]  # Dimension BxZxN'xN
        b_p = self.prior_intercept.numpy()[None, ..., None]  # Dimension BxZxN'xN
        b_bar = (b_q - b_p)  # Dimension BxZxN'xN

        D_inv = self.prc_cov_inv.detach().numpy()[None, ...]  # Dimension BxZxNxN'
        mu = self.posterior_mean(t)[None, ..., None]  # Dimension BxZxN'xN

        q_z = self.posterior_mjp(t)[None, :, None, None]  # Dimension BxZxNxN'

        # Compute r.h.s., cf. appendix_notes Sect. 2.1
        res = (q_z * -1 * (quad_form(A_bar, D_inv, keepdims=True) @ mu + quad_form(A_bar, D_inv, b_bar,
                                                                                   keepdims=True)) - A_q_T @ lambda_).squeeze(
            -1)  # Dimension BxZxN
        if is_batch:
            res = res.reshape((num_batches, -1)).T  # Dimension Z*NxB
        else:
            res = res.reshape((num_batches, -1)).T.flatten()  # Dimension Z*N
        return res

    def _ode_psi(self, t: float, Psi: np.ndarray) -> np.ndarray:
        """
        Computes r.h.s. of the ODE for the costate/lagrange multiplier for the posterior covariance dynamics
        (Note: input and output is flattend over ZxNxN' dimensions
          + batchwise vectorization over Psi is implemented)

        :param t: Evaluation time point
        :param Psi:  [n_states*n_dim*n_dim] or [n_states*n_dim*n_dim x num_batches] Current covaraince costate
        :return: r.h.s. of ODE
        """
        if Psi.ndim == 2:
            is_batch = True
            num_batches = Psi.shape[1]
        else:
            is_batch = False
            num_batches = 1

        # Reshape variable to the original space ZxNxN'
        Psi = Psi.T.reshape((num_batches, self.n_states, self.n_dim, self.n_dim))  # Dimension BxZxNxN'

        # Set variables
        A_q = self.variational_slope(t)[None, ...]  # Dimension BxZxNxN'
        A_q_T = np.moveaxis(A_q, -2, -1)  # Dimension BxZxN'xN
        A_p = self.prior_slope.numpy()[None, ...]  # Dimension BxZxNxN'
        A_bar = A_q - A_p  # Dimension BxZxNxN'

        D_inv = self.prc_cov_inv.detach().numpy()[None, ...]  # Dimension BxZxNxN'

        q_z = self.posterior_mjp(t)[None, :, None, None]  # Dimension BxZxNxN'

        # Compute r.h.s., cf. appendix_notes Sect. 2.1
        res = -.5 * q_z * quad_form(A_bar, D_inv, keepdims=True) - A_q_T @ Psi - Psi @ A_q  # BxZxNxN'

        if is_batch:
            res = res.reshape((num_batches, -1)).T  # Dimension Z*N*N'xB
        else:
            res = res.reshape((num_batches, -1)).T.flatten()  # Dimension Z*N*N'

        return res

    def update_mjp_costate(self):
        """
        Performs the backward integration for the costate/lagrange mutlipliers nu(t) corresponding to the mjp marginals
        """
        # Initial costate is zero and has the dimensionalities correspdoning to the mjp marginals q_z(t)
        initial_mjp_costate = np.zeros(self.n_states)

        # Create piecewise ode solutions between data points
        mjp_costate_list = []

        # Solve ODE backward in time
        for interval in reversed(range(self.n_intervals)):
            t_span = self.interval_boundaries[interval + 1], self.interval_boundaries[interval]

            cur_mjp_costate = integrate.solve_ivp(self._ode_nu, t_span, initial_mjp_costate,
                                                  method='BDF', dense_output=True, vectorized=True)

            mjp_costate_list.append(cur_mjp_costate.sol)

            # Reset Conditions for costate
            if interval > 0:
                # Set variables

                # observation
                t_i = self.t_obs[interval - 1]
                x_i = self.obs_data[:, interval - 1].numpy()[None, ...]  # Dimension ZxN

                # variational parameters
                mu = self.posterior_mean(t_i)  # Dimension ZxN
                Sigma = self.posterior_cov(t_i)  # Dimension ZxNxN'
                Sigma_obs_inv = self.obs_cov_inv.detach().numpy()[None, ...]  # Dimension ZxNxN'
                Sigma_obs_logdet = self.obs_cov_logdet.detach().numpy()  # Dimension ZxNxN'
                nu = cur_mjp_costate.sol(t_i)  # Dimension Z

                x_mu_diff = x_i - mu  # Dimension ZxN

                # Reset conditions, cf. appendix_notes Sect. 3
                initial_mjp_costate = nu + .5 * (self.n_dim * np.log(2 * np.pi) + Sigma_obs_logdet + quad_form(
                    x_mu_diff, Sigma_obs_inv) + np.sum(Sigma_obs_inv * Sigma,
                                                       axis=(1, 2)))  # Dimension Z

        # Concatenate Piecewise Solutions
        self.mjp_costate = cat_ode_solutions(mjp_costate_list, shape=(self.n_states))

    def _ode_nu(self, t: float, nu: np.ndarray):
        """
        Computes r.h.s. of the ODE for the costate/lagrange multiplier for the posterior mjp dynamics

        (Note: batchwise vectorization over nu is implemented)

        :param t: Evaluation time point
        :param nu: [n_states] or [n_states x num_batches]  Current mjp costate
        :return: r.h.s. of ODE
        """

        if nu.ndim == 2:
            is_batch = True
            num_batches = nu.shape[1]
        else:
            is_batch = False
            num_batches = 1

        # Reshape nu
        nu = nu.T.reshape((num_batches, self.n_states))  # Dimension BxZ

        # Set variables
        A_q = self.variational_slope(t)[None, ...]  # Dimension BxZxNxN'
        A_p = self.prior_slope.numpy()[None, ...]  # Dimension ZxNxN'
        A_bar = A_q - A_p  # Dimension BxZxNxN'

        b_q = self.variational_intercept(t)[None, ..., None]  # Dimension BxZxNxN'
        b_p = self.prior_intercept.numpy()[None, ..., None]  # Dimension BxZxNxN'
        b_bar = b_q - b_p  # Dimension BxZxNxN'

        D_inv = self.prc_cov_inv.detach().numpy()[None, ...]  # Dimension BxZxNxN'

        mu = self.posterior_mean(t)[None, ..., None]  # Dimension BxZxN'xN
        Sigma = self.posterior_cov(t)[None, ...]  # Dimension BxZxNxN'

        variational_log_rate_mtx = self.variational_log_rate_mtx(t)[None, ...]  # Dimension BxZxZ'
        variational_rate_mtx = self.variational_rate_mtx(t)[None, ...]  # Dimension BxZxZ'
        prior_rate_mtx = deepcopy(self.prior_rate_mtx.numpy()[None, ...])  # Dimension BxZxZ'
        np.einsum('ijj->ij', prior_rate_mtx)[:] = 1.0

        # Compute terms; for res_1, res_2 cf. appendix_notes Sect. 1.2, for res_3 Sect. 3
        # Compute res_1=(A_bar@mu+b_bar)^T D_inv (A_bar@mu+b_bar)
        A_mu_b = A_bar @ mu + b_bar  # Dimension BxZxNxN'
        res_1 = quad_form(A_mu_b, D_inv)  # Dimension BxZ

        # Compute res_2=trace(A_bar^T @ D_inv @ A_bar @ Sigma)
        res_2 = np.sum(A_bar * D_inv @ A_bar @ Sigma, axis=(2, 3))  # Dimension BxZ

        # Compute res_3=sum_{z' \neq z} #TODO clipping?? make consistent and do not use nu
        res_3_mtx = variational_rate_mtx * (
                variational_log_rate_mtx - np.log(prior_rate_mtx) - 1.0 + nu[:, :, None] - nu[:, None,
                                                                                           :]) + prior_rate_mtx  # Dimension BxZx Z'
        res_3_mtx_diag = np.einsum('ijj->ij', res_3_mtx)  # Dimension B x Z
        res_3_mtx_diag[:] = 0.0  # Dimension B x Z
        res_3 = np.sum(res_3_mtx, axis=2)  # Dimension BxZ

        # Compute r.h.s.
        res = res_1 + res_2 + res_3

        if is_batch:
            res = res.T  # Dimension ZxB
        else:
            res = res.T.flatten()  # Dimension Z

        return res

    # ------------------------------------------------------
    # ----- 1.2) Posterior Initial Condition updates -------
    # ------------------------------------------------------
    def update_posterior_initial_conditions(self, max_steps: int, max_iter: int, epsrel: float,
                                            decay: float):  # TODO doc
        # -------------------- 1. Update initial posterior MJP --------------------
        p_z = torch.as_tensor(self.initial_prior_mjp, dtype=torch.float64)
        nu = torch.as_tensor(self.mjp_costate(0.0), dtype=torch.float64)
        kl = torch.as_tensor(gaussian_kl(mu_q=self.initial_posterior_mean, Sigma_q=self.initial_posterior_cov.detach(),
                         mu_p=self.initial_prior_mean, Sigma_p=self.initial_prior_cov).detach(), dtype=torch.float64)

        elbo_old = self.elbo()
        elbo_new = torch.tensor(-float('inf'))
        n_steps = 1
        n_iter = 1
        terminated = False
        while n_steps <= max_steps and terminated is False:
            # Compute gradient w.r.t. q_z
            q_z_params_cur = self._initial_posterior_mjp_params.detach().clone()
            q_z = self.initial_posterior_mjp

            if self._initial_posterior_mjp_params.grad is not None:
                self._initial_posterior_mjp_params.grad.zero_()

            loss = (q_z * torch.log(q_z/p_z) + q_z * kl + nu @ q_z).sum()
            loss.backward()

            gradient = self._initial_posterior_mjp_params.grad

            while elbo_new < elbo_old and n_iter <= max_iter:
                scaled_gradient = decay ** n_iter * gradient
                initial_posterior_mjp_params_new = deepcopy(q_z_params_cur)
                initial_posterior_mjp_params_new -= scaled_gradient  # Note: self._initial_posterior_mjp_params is in log-space

                exp_sum = initial_posterior_mjp_params_new.exp().sum()
                if exp_sum > 1. - EPS:
                    pseudonormalizer = torch.log1p(-1. * torch.tensor(EPS)) - torch.logsumexp(initial_posterior_mjp_params_new, 0)
                    initial_posterior_mjp_params_new += pseudonormalizer
                else:
                    pass

                # `level out' the params, i.e. clip to EPS and add the `difference' to the maximum probability entry
                entries_with_prob_below_eps = np.where(initial_posterior_mjp_params_new.exp() < EPS)[0]

                initial_posterior_mjp_params_new[entries_with_prob_below_eps] = np.log(EPS)
                if len(entries_with_prob_below_eps) == len(initial_posterior_mjp_params_new):
                    pass
                else:
                    max_prob_idx = initial_posterior_mjp_params_new.exp().argmax()
                    initial_posterior_mjp_params_new[max_prob_idx] = torch.log(initial_posterior_mjp_params_new.exp()[max_prob_idx] - len(entries_with_prob_below_eps) * EPS)

                self._initial_posterior_mjp_params.data = torch.as_tensor(initial_posterior_mjp_params_new, dtype=torch.float64)
                self.update_posterior()
                elbo_new = self.elbo()
                n_iter += 1

            if elbo_new < elbo_old and n_iter == max_iter + 1:
                self._initial_posterior_mjp_params.data = torch.as_tensor(q_z_params_cur, dtype=torch.float64)
                self.update_posterior()
                terminated = True

            if (elbo_new - elbo_old).abs() / elbo_old.abs() < epsrel:
                break
            elif not terminated:
                elbo_old = deepcopy(elbo_new)
                elbo_new = torch.tensor(-float('inf'))
                n_steps += 1
                n_iter = 1

        #q_z = self.initial_posterior_mjp.detach()
        #for iter in range(max_iter):
        #    q_z_old = q_z.clone()

        #    for z in range(self.n_states - 1):
        #        q_neq_z = q_z[:-1][torch.arange(self.n_states - 1) != z]
        #        q_z[z] = (1.0 - q_neq_z.sum()) / (
        #                1.0 + p_z[-1] / p_z[z] * torch.exp(nu[z] - nu[-1] + kl[z] - kl[-1]))

        #    q_z[-1] = 1.0 - q_z[:-1].sum()

        #    # Clamp for numerical stability
        #    q_z = torch.clamp(q_z, EPS, 1 - EPS)

        #    if (q_z_old - q_z).abs().sum() < epsrel:
        #        break

        #self.initial_posterior_mjp = torch.as_tensor(q_z, dtype=torch.float64)
        #self.update_posterior()

        # -------------------- 2. Update initial posterior mean --------------------
        mu_p = self.initial_prior_mean[..., None]  # Dimension ZxNxN'
        lambda_ = torch.as_tensor(self.mean_costate(0.0), dtype=torch.float)[:, None, :]  # Dimension ZxN'xN
        Sigma_p = self.initial_prior_cov  # Dimension ZxNxN'
        Sigma_p_inv = torch.inverse(Sigma_p)
        q_z = self.initial_posterior_mjp.detach()  # Dimension Z

        elbo_old = self.elbo()
        elbo_new = torch.tensor(-float('inf'))
        n_steps = 1
        n_iter = 1
        terminated = False
        while n_steps <= max_steps and terminated is False:
            # Compute gradient w.r.t. mu_0
            mu_q_cur = self.initial_posterior_mean.detach().clone()
            mu_q = self.initial_posterior_mean  # Dim ZxN

            if mu_q.grad is not None:
                mu_q.grad.zero_()
            loss = ((.5 * q_z * quad_form(mu_p - mu_q[..., None], Sigma_p_inv)) + (lambda_ @ mu_q[..., None]).squeeze()).sum()
            loss.backward()

            gradient = self._initial_posterior_mean.grad

            decay_mean = decay * torch.as_tensor(self.posterior_mjp(0.0)[:, None], dtype=torch.float)  # Dim Zx1
            while elbo_new < elbo_old and n_iter <= max_iter:
                scaled_gradient = decay_mean ** n_iter * gradient
                initial_posterior_mean_new = mu_q_cur - scaled_gradient

                self.initial_posterior_mean = initial_posterior_mean_new
                self.update_posterior()
                elbo_new = self.elbo()
                n_iter += 1

            if elbo_new < elbo_old and n_iter == max_iter + 1:
                self.initial_posterior_mean = mu_q_cur
                self.update_posterior()
                terminated = True

            if (elbo_new - elbo_old).abs() / elbo_old.abs() < epsrel:  # TODO these kind of criteria allow the elbo to decrease (bc of abs)
                break
            elif not terminated:
                elbo_old = deepcopy(elbo_new)
                elbo_new = torch.tensor(-float('inf'))
                n_steps += 1
                n_iter = 1

        # -------------------- 3. Update initial posterior covariance --------------------
        Sigma_p_logdet = torch.logdet(Sigma_p)
        Sigma_p_inv = torch.inverse(Sigma_p)
        Psi = torch.as_tensor(self.cov_costate(0.0), dtype=torch.float)

        elbo_old = self.elbo()
        elbo_new = torch.tensor(-float('inf'))
        n_steps = 1
        n_iter = 1
        terminated = False
        while n_steps <= max_steps and terminated is False:
            # Compute gradient w.r.t. Sigma_0
            Sigma_q_cur = self.initial_posterior_cov.detach().clone()
            Sigma_q_cov_chol_cur = self.initial_posterior_cov_chol.detach().clone()

            Sigma_q = self.initial_posterior_cov
            Sigma_q_cov_chol = self.initial_posterior_cov_chol
            Sigma_q_logdet = self.initial_posterior_cov_logdet

            if Sigma_q_cov_chol.grad is not None:
                Sigma_q_cov_chol.grad.zero_()

            loss = (.5 * q_z * (
                    Sigma_p_logdet - Sigma_q_logdet + (Sigma_p_inv * Sigma_q).sum(dim=(1, 2))) + (
                            Psi * Sigma_q).sum(dim=(1, 2))).sum()
            loss.backward()

            gradient = self.initial_posterior_cov_chol.grad

            decay_cov = decay * torch.as_tensor(self.posterior_mjp(0.0)[:, None, None], dtype=torch.float)  # Dim Zx1x1
            while elbo_new < elbo_old and n_iter <= max_iter:
                scaled_gradient = decay_cov ** n_iter * gradient
                Sigma_q_cov_chol_new = Sigma_q_cov_chol_cur - scaled_gradient

                self.initial_posterior_cov_chol = Sigma_q_cov_chol_new
                self.update_posterior()
                elbo_new = self.elbo()
                n_iter += 1

            if elbo_new < elbo_old and n_iter == max_iter + 1:
                self.initial_posterior_cov = Sigma_q_cur
                self.update_posterior()
                terminated = True

            if (elbo_new - elbo_old).abs() / elbo_old.abs() < epsrel:
                break
            elif not terminated:
                elbo_old = deepcopy(elbo_new)
                elbo_new = torch.tensor(-float('inf'))
                n_steps += 1
                n_iter = 1

    def update_diffusion_posterior_initial_conditions(self, max_iter: int, epsrel: float):
        """
        Updates the variational initial conditions for the variational diffusion process (mu_q(z,0), Sigma_q(z,0))
        Uses closed form update for mu_q(z,0) and gradient update for Sigma_q(z,0)

        :param max_iter: maximum number of gradient steps, for Sigma_q(z,0) optimization
        :param epsrel: convergence criterion --> relative tolerance in loss for Sigma_q(z,0) optimization
        """

        # update initial_posterior_mean
        mu_p = self.initial_prior_mean[..., None]  # Dimension ZxNxN'
        lambda_ = torch.as_tensor(self.mean_costate(0.0), dtype=torch.float)[:, None, :]  # Dimension ZxN'xN
        Sigma_p = self.initial_prior_cov  # Dimension ZxNxN'
        Sigma_p_inv = torch.inverse(Sigma_p)
        q_z = self.initial_posterior_mjp.detach()  # Dimension Z

        def closure_mean():
            self.optimizer.zero_grad()
            mu_q = self.initial_posterior_mean[..., None]  # Dim ZxNxN'
            loss = ((.5 * q_z * quad_form(mu_p - mu_q, Sigma_p_inv)) + (lambda_ @ mu_q).squeeze()).sum()
            loss.backward()
            return loss

        loss = torch.tensor(float('inf'))
        for iter in range(max_iter):
            loss_old = loss.clone()
            loss = self.optimizer.step(closure_mean)
            if (loss - loss_old).abs() / loss_old.abs() < epsrel:
                break

        # update initial_posterior_cov using gradient updates
        Sigma_p_logdet = torch.logdet(Sigma_p)
        Sigma_p_inv = torch.inverse(Sigma_p)
        Psi = torch.as_tensor(self.cov_costate(0.0), dtype=torch.float)

        def closure_cov():
            self.optimizer.zero_grad()
            Sigma_q = self.initial_posterior_cov
            Sigma_q_logdet = self.initial_posterior_cov_logdet

            loss = (.5 * q_z * (
                    Sigma_p_logdet - Sigma_q_logdet + (Sigma_p_inv * Sigma_q).sum(dim=(1, 2))) + (
                            Psi * Sigma_q).sum(dim=(1, 2))).sum()  # TODO @ does not give trace?
            loss.backward()
            return loss

        # optimize until convergence
        loss = torch.tensor(float('inf'))
        for iter in range(max_iter):
            loss_old = loss.clone()
            loss = self.optimizer.step(closure_cov)
            if (loss - loss_old).abs() / loss_old.abs() < epsrel:
                break

    def update_mjp_posterior_initial_conditions(self, max_iter: int, epsrel: float, coordinate_update: bool = True):
        """
        Updates the initial condition for the variational MJP (q(z,0)) using a coordinate ascent algorithm or a
        gradient algorithm

        :param max_iter maximum number of coordinate or gradient steps
        :param epsrel convergence criteria --> relative change in parameter for coordinate update or relative change
                        in loss for gradient update
        :param coordinate_update flag if coordinate update or gradient update should be used
        """

        p_z = self.initial_prior_mjp
        nu = torch.as_tensor(self.mjp_costate(0.0), dtype=torch.float)
        kl = gaussian_kl(mu_q=self.initial_posterior_mean, Sigma_q=self.initial_posterior_cov.detach(),
                         mu_p=self.initial_prior_mean, Sigma_p=self.initial_prior_cov)

        # DO a coordinate ascent algorithm
        if coordinate_update:
            q_z = self.initial_posterior_mjp.detach()
            for iter in range(max_iter):
                q_z_old = q_z.clone()

                for z in range(self.n_states - 1):
                    q_neq_z = q_z[:-1][torch.arange(self.n_states - 1) != z]
                    q_z[z] = (1.0 - q_neq_z.sum()) / (
                            1.0 + p_z[-1] / p_z[z] * torch.exp(nu[z] - nu[-1] + kl[z] - kl[-1]))

                q_z[-1] = 1.0 - q_z[:-1].sum()

                # Clamp for numerical stability
                q_z = torch.clamp(q_z, EPS, 1 - EPS)

                if (q_z_old - q_z).abs().sum() < epsrel:
                    break

            self.initial_posterior_mjp = q_z
        # DO a gradient algorithm
        else:
            def closure():
                self.optimizer.zero_grad()
                q_z = self.initial_posterior_mjp
                loss = discrete_kl(q_z, p_z) + (q_z * kl).sum() + (q_z * nu).sum()
                loss.backward()
                return loss

            # optimize until convergence
            loss = torch.tensor(float('inf'))
            for iter in range(max_iter):
                loss_old = loss.clone()
                loss = self.optimizer.step(closure)
                if (loss - loss_old).abs() / loss_old.abs() < epsrel:
                    break

    # --------------------------------------------
    # ----- 1.3) Posterior forward updates -------
    # --------------------------------------------
    def update_posterior(self):
        """
        Solve forward ODEs for the moments of the diffusion marginal distributions, i.e. the mean, covariance
        of the conditional Gaussians N(y|z) and for the marginal distribution of the mjp process q_z(t)
        (time inhomogeneous master equation). Updates the variational slope, intercept, expected rates and expected
        sojourn time parameters based on the solution
        """

        # Get initial states
        initial_posterior_mean = self.initial_posterior_mean.detach().numpy().flatten()
        initial_posterior_cov = self.initial_posterior_cov.detach().numpy().flatten()
        initial_posterior_mjp = self.initial_posterior_mjp.detach().numpy()
        initial_expected_rates = np.zeros(self.n_states ** 2)
        initial_expected_sojourn_times = np.zeros(self.n_states)

        # Concetante initial states for joint ODE forward system
        initial_joint_state = np.concatenate((initial_posterior_mean,
                                              initial_posterior_cov,
                                              initial_posterior_mjp,
                                              initial_expected_rates,
                                              initial_expected_sojourn_times))

        # Solve joint ODE forward in time
        joint_solution_list = []
        for interval in range(self.n_intervals):
            t_span = self.interval_boundaries[interval], self.interval_boundaries[interval + 1]
            joint_solution = integrate.solve_ivp(self._ode_joint_posterior, t_span, initial_joint_state,
                                                 method='BDF', dense_output=True, vectorized=True)

            joint_solution_list.append(joint_solution.sol)
            initial_joint_state = joint_solution.sol(
                t_span[
                    1])  # Dimension (n_states*n_dim + n_states*n_dim*n_dim + n_states + n_states*n_states + n_states)xB

        joint_solution = cat_ode_solutions(joint_solution_list, shape=(
                self.n_states * self.n_dim + self.n_states * self.n_dim ** 2 + self.n_states + self.n_states ** 2 + self.n_states))

        # Extract and reshape solutions
        numel_mu = self.n_states * self.n_dim
        numel_Sigma = self.n_states * self.n_dim ** 2
        numel_q_z = self.n_states
        numel_expected_rates = self.n_states ** 2

        # expected rates and expected sojourn times
        _, _, _, expected_rates, expected_sojourn_times = np.split(joint_solution(self.t_span[1]), [numel_mu,
                                                                                                    numel_mu + numel_Sigma,
                                                                                                    numel_mu + numel_Sigma + numel_q_z,
                                                                                                    numel_mu + numel_Sigma + numel_q_z + numel_expected_rates])

        self._expected_posterior_rates = torch.as_tensor(expected_rates.reshape(self.n_states, self.n_states),
                                                         dtype=torch.float)
        self._expected_posterior_sojourn_times = torch.as_tensor(expected_sojourn_times, dtype=torch.float)

        # updates variational mean and covariance by reshaping solutions from flattened to original space
        self.posterior_mean = ReshapedOdeSolution(joint_solution, shape=(self.n_states, self.n_dim),
                                                  sub_idxs=[0, numel_mu])
        self.posterior_cov = ReshapedOdeSolution(joint_solution, shape=(self.n_states, self.n_dim, self.n_dim),
                                                 sub_idxs=[numel_mu, numel_mu + numel_Sigma])

        # extract the master equation solution as OdeSolution
        self.posterior_mjp = ReshapedOdeSolution(joint_solution, shape=(self.n_states,),
                                                 sub_idxs=[numel_mu + numel_Sigma,
                                                           numel_mu + numel_Sigma + numel_q_z])  # TODO clip param

    def _ode_joint_posterior(self, t: float, joint_state: np.ndarray):
        """
        Computes r.h.s. of the ODE for the posterior moments and expected sufficient stats (mean, covariance of the
        diffusion, marginal probabilities of the MJP, expected rates expected sujourn times)

        (Note: input and output is flattened over concatenated dimensions + batchwise vectorization is implemented)

        :param t: Evaluation time point
        :param joint_state: [n_states*n_dim+n_states*n_dim*n_dim+n_states+n_states*n_states+n_states] or
                                [n_states*n_dim+n_states*n_dim*n_dim+n_states+n_states*n_states+n_states x num_batches]
                                Current mean, covariance, mjp posterior, rate and sojourn time expectation
        :return: r.h.s. of ODE
        """

        # Split quantities
        numel_mu = self.n_states * self.n_dim
        numel_Sigma = self.n_states * self.n_dim ** 2
        numel_q_z = self.n_states
        mu, Sigma, q_z, _ = np.split(joint_state,
                                     [numel_mu, numel_mu + numel_Sigma, numel_mu + numel_Sigma + numel_q_z])

        if joint_state.ndim == 2:
            is_batch = True
            num_batches = joint_state.shape[1]
        else:
            is_batch = False
            num_batches = 1

        # Reshape variable to the original space ZxN
        mu = mu.T.reshape((num_batches, self.n_states, self.n_dim))[..., None]  # Dimension BxZxN'xN
        # Reshape variable to the original space ZxNxN'
        Sigma = Sigma.T.reshape((num_batches, self.n_states, self.n_dim, self.n_dim))  # Dimension BxZxNxN'
        # Reshape variable to the original space Z
        q_z = q_z.T.reshape((num_batches, self.n_states))  # Dimension BxZ

        # Set variables
        variational_rate_mtx = self.variational_rate_mtx(t)[None, ...]  # Dimension BxZxZ'

        A_q = self.variational_slope(t)[None, ...]  # Dimension BxZxNxN'
        b_q = self.variational_intercept(t)[None, ..., None]  # Dimension BxZxNxN'
        A_q_T = np.moveaxis(A_q, -2, -1)  # Dimension BxZxN'xN
        D = self.prc_cov.detach().numpy()[None, ...]  # Dimension BxZxNxN'

        # Compute r.h.s. of mu, cf. appendix_notes Sect. 1.2
        rhs_mu = (A_q @ mu + b_q).squeeze(-1).reshape((num_batches, -1))  # Dimension BxZ*N

        # Compute r.h.s. of Sigma, cf. appendix_notes Sect. 1.2
        rhs_Sigma = (A_q @ Sigma + Sigma @ A_q_T + D).reshape((num_batches, -1))  # Dimension BxZ*N*N'

        # Compute r.h.s. of q_z, cf. appendix_notes Sect 1.2
        rhs_q_z = np.sum(q_z[..., None] * variational_rate_mtx, axis=1)  # Dimension BxZ

        # Compute r.h.s. of expected rates, cf. appendix_notes Sect 1.2
        rhs_expected_rates = (q_z[..., None] * variational_rate_mtx).reshape((num_batches, -1))  # Dimension Bx Z'* Z''

        # Compute r.h.s. of expected soujourn times, cf. appendix_notes Sect 1.2
        rhs_expected_sojourn_times = q_z  # Dimension BxZ'''

        # Concatenate Solution
        res = np.concatenate((rhs_mu, rhs_Sigma, rhs_q_z, rhs_expected_rates, rhs_expected_sojourn_times),
                             axis=1)  # Dimension B x Z*N+Z*N*N'+Z+Z'*Z''+Z'''

        if is_batch:
            res = res.T  # Dimension  Z*N+Z*N*N'+Z+Z'*Z''+Z''' x B
        else:
            res = res.squeeze()  # Dimension  Z*N+Z*N*N'+Z+Z'*Z''+Z'''

        return res

    # ------------------------------------------------------
    # ----------- 1.4) Variational parameter updates -------
    # ------------------------------------------------------
    def update_variational_params(self, max_steps: int, max_iter: int, epsrel: float, decay: float):
        """
        Updates the variational parameters via gradient ascent

        :param max_steps: specifying the maximum number of gradient steps
        :param max_iter: specifying the maximum number of line search iterations per step
        """
        logging.debug('Updating Variational Params')
        self.update_variational_rates(max_steps, max_iter, epsrel, decay)
        self.update_variational_slope(max_steps, max_iter, epsrel, decay)
        self.update_variational_intercept(max_steps, max_iter, epsrel, decay)

    def update_variational_rates(self, max_steps: int, max_iter: int, epsrel: float, decay: float):  # TODO Doc

        log_clip = np.log(self.rate_clip)

        elbo_old = self.elbo()
        elbo_new = torch.tensor(-float('inf'))
        n_steps = 1
        n_iter = 1
        terminated = False
        time_grid = self.variational_log_rate_mtx.time_grid
        while n_steps <= max_steps and terminated is False:
            interpolants_old = self.variational_log_rate_mtx.interpolants
            gradient = self.variational_log_rate_mtx.gradient(prior_rate_mtx=self.prior_rate_mtx.numpy(),
                                                              posterior_mjp=self.posterior_mjp,
                                                              mjp_costate=self.mjp_costate)
            while elbo_new < elbo_old and n_iter <= max_iter:
                scaled_gradient = decay ** n_iter * gradient
                interpolants_new = interpolants_old - scaled_gradient

                self.variational_log_rate_mtx = VariationalLogRateMtx(
                    prior_rate_mtx_zerodiag=self.prior_rate_mtx_zerodiag,
                    interpolants=interpolants_new,
                    time_grid=time_grid,
                    clip_above=log_clip)
                self.update_posterior()
                elbo_new = self.elbo()
                n_iter += 1

            if elbo_new < elbo_old and n_iter == max_iter + 1:
                self.variational_log_rate_mtx = VariationalLogRateMtx(
                    prior_rate_mtx_zerodiag=self.prior_rate_mtx_zerodiag,
                    time_grid=time_grid,
                    interpolants=interpolants_old,
                    clip_above=log_clip)
                self.update_posterior()
                terminated = True

            if (elbo_new - elbo_old).abs() / elbo_old.abs() < epsrel:
                break
            elif not terminated:
                elbo_old = deepcopy(elbo_new)
                elbo_new = torch.tensor(-float('inf'))
                n_steps += 1
                n_iter = 1

    def update_variational_slope(self, max_steps: int, max_iter: int, epsrel: float, decay: float):

        elbo_old = self.elbo()
        elbo_new = torch.tensor(-float('inf'))
        n_steps = 1
        n_iter = 1
        terminated = False
        time_grid = self.variational_slope.time_grid
        while n_steps <= max_steps and terminated is False:
            interpolants_old = self.variational_slope.interpolants
            gradient = self.variational_slope.gradient(prc_cov_inv=self.prc_cov_inv.detach().numpy(),
                                                       prior_slope=self.prior_slope.numpy(),
                                                       prior_intercept=self.prior_intercept.numpy(),
                                                       variational_intercept=self.variational_intercept,
                                                       posterior_mean=self.posterior_mean,
                                                       posterior_cov=self.posterior_cov,
                                                       posterior_mjp=self.posterior_mjp,
                                                       mean_costate=self.mean_costate,
                                                       cov_costate=self.cov_costate)  # Dim ZxNxN'xT

            decay = decay * self.posterior_mjp(self.variational_slope.time_grid)[:, None, None, :]  # Dim Zx1x1xT
            while elbo_new < elbo_old and n_iter <= max_iter:
                scaled_gradient = decay ** n_iter * gradient

                interpolants_new = interpolants_old - scaled_gradient
                variational_slope_new = VariationalSlope(prior_slope=self.prior_slope.numpy(),
                                                         interpolants=interpolants_new,
                                                         time_grid=time_grid,
                                                         slope_clip=self.slope_clip)
                self.variational_slope = variational_slope_new
                self.update_posterior()
                elbo_new = self.elbo()
                n_iter += 1
            if elbo_new < elbo_old and n_iter == max_iter + 1:
                variational_slope_new = VariationalSlope(prior_slope=self.prior_slope.numpy(),
                                                         interpolants=interpolants_old,
                                                         time_grid=time_grid,
                                                         slope_clip=self.slope_clip)
                self.variational_slope = variational_slope_new
                self.update_posterior()
                terminated = True

            if (elbo_new - elbo_old).abs() / elbo_old.abs() < epsrel:
                break
            elif not terminated:
                elbo_old = deepcopy(elbo_new)
                elbo_new = torch.tensor(-float('inf'))
                n_steps += 1
                n_iter = 1

    def update_variational_intercept(self, max_steps: int, max_iter: int, epsrel: float, decay: float):

        elbo_old = self.elbo()
        elbo_new = torch.tensor(-float('inf'))
        n_steps = 1
        n_iter = 1
        terminated = False
        time_grid = self.variational_intercept.time_grid
        while n_steps <= max_steps and terminated is False:
            interpolants_old = self.variational_intercept.interpolants
            gradient = self.variational_intercept.gradient(prc_cov_inv=self.prc_cov_inv.detach().numpy(),
                                                           prior_slope=self.prior_slope.numpy(),
                                                           prior_intercept=self.prior_intercept.numpy(),
                                                           variational_slope=self.variational_slope,
                                                           posterior_mean=self.posterior_mean,
                                                           posterior_mjp=self.posterior_mjp,
                                                           mean_costate=self.mean_costate)

            decay = decay * self.posterior_mjp(self.variational_slope.time_grid)[:, None, :]  # Dim Zx1xT
            while elbo_new < elbo_old and n_iter <= max_iter:
                scaled_gradient = decay ** n_iter * gradient
                interpolants_new = interpolants_old - scaled_gradient
                variational_intercept_new = VariationalIntercept(prior_intercept=self.prior_intercept.numpy(),
                                                                 interpolants=interpolants_new,
                                                                 time_grid=time_grid)
                self.variational_intercept = variational_intercept_new
                self.update_posterior()
                elbo_new = self.elbo()
                n_iter += 1
            if elbo_new < elbo_old and n_iter == max_iter + 1:
                variational_intercept_new = VariationalIntercept(prior_intercept=self.prior_intercept.numpy(),
                                                                 interpolants=interpolants_old,
                                                                 time_grid=time_grid)
                self.variational_intercept = variational_intercept_new
                self.update_posterior()
                terminated = True

            if (elbo_new - elbo_old).abs() / elbo_old.abs() < epsrel:
                break
            elif not terminated:
                elbo_old = deepcopy(elbo_new)
                elbo_new = torch.tensor(-float('inf'))
                n_steps += 1
                n_iter = 1

    # --------------------------------------------
    # ---------- 2) Prior updates ---------------
    # --------------------------------------------
    def update_prior_params(self, max_steps: int, max_iter: int, decay:float, epsrel_elbo: float, epsrel_int: float,
                            obs_cov_closed_form: bool = True, update_obs_cov: bool = True,
                            update_dispersion: bool = True, mode_specific_dispersion: bool = False):
        """
        Updates the prior parameters using Variational EM

        TODO doc
        :param epsrel_int: float specifying the required accuracy for integration
        :param epsrel_elbo: float, usual elbo stopping criterion
        :param max_iter: int specifying the maximum number of gradient steps
        :param epsrel: numerical accuracy for update_prior_diffusion -> see update_prior_diffusion
        """
        logging.debug('Updating Prior Params')
        if update_obs_cov:
            self.update_prior_obs_model(closed_form_update=obs_cov_closed_form, max_steps=max_steps, max_iter=max_iter, decay=decay, epsrel=epsrel_elbo)
        self.update_prior_diffusion(max_steps=max_steps, max_iter=max_iter, decay=decay, epsrel_elbo=epsrel_elbo,
                                    epsrel_int=epsrel_int)
        self.update_prior_mjp()
        if update_dispersion:
            self.update_prior_dispersion(max_steps=max_steps, max_iter=max_iter, decay=decay, epsrel_int=epsrel_int,
                                         epsrel_elbo=epsrel_elbo, mode_specific_dispersion=mode_specific_dispersion)

    # ---------- 2.1) Observation Model updates ---------------
    def update_prior_obs_model(self, max_steps: int, max_iter: int, decay: float, epsrel: float, closed_form_update: bool = True):
        """
        Updates the prior parameters of the observation model
        :param max_iter:
        :param decay:
        :param epsrel:
        :param max_steps:
        """
        logging.debug('Updating Prior Observation Model')
        self.update_obs_cov(closed_form_update=closed_form_update, max_steps=max_steps, max_iter=max_iter, decay=decay, epsrel=epsrel)

    def update_obs_cov(self, max_steps: int, max_iter: int, decay: float, epsrel: float, closed_form_update: bool = True):
        """
        Update the observation covariance with the closed-form MLE, cf. appendix_notes Sect. 4.2

        # TODO doc
        :return: None
        """
        if closed_form_update:
            # term 1, E_z[(x-mu)(x-mu)^T]
            x = self.obs_data.permute(1, 0)[:, None, :]  # Dimension TxZxN
            mu = torch.as_tensor(self.posterior_mean(self.t_obs), dtype=torch.float).permute(2, 0, 1)  # Dimension TxZxN
            x_mu_diff = (x - mu)[..., None]  # Dimension TxZxNxN'
            x_mu_diff_T = x_mu_diff.transpose(-1, -2)  # Dimension TxZxN'xN
            outer = x_mu_diff @ x_mu_diff_T  # Dimension TxZxNxN

            q_z = torch.as_tensor(self.posterior_mjp(self.t_obs), dtype=torch.float).permute(1, 0)[
               ..., None, None]  # Dimension TxZxNxN
            state_averaged_outer = (q_z * outer).sum(dim=1)  # Dimension TxNxN

            # term 2, E_z[Sigma]
            Sigma_q = torch.as_tensor(self.posterior_cov(self.t_obs), dtype=torch.float).permute(3, 0, 1,
                                                                                                2)  # Dimension TxZxNxN
            state_averaged_covariance = (q_z * Sigma_q).sum(dim=1)  # Dimension TxNxN

            Sigma_obs = torch.mean(state_averaged_outer + state_averaged_covariance, dim=0)  # Dimension NxN
            self.obs_cov = Sigma_obs

        else:

            elbo_old = self.elbo()
            elbo_new = torch.tensor(-float('inf'))
            n_steps = 1
            n_iter = 1
            terminated = False
            while n_steps <= max_steps and terminated is False:

                Sigma_chol_cur = self.obs_cov_chol.detach().clone()

                Sigma_chol = self.obs_cov_chol
                Sigma_logdet = self.obs_cov_logdet
                Sigma_inv = self.obs_cov_inv[None, None, ...]  # Dim TxZxDxD
                x = self.obs_data.permute(1,0)[:, None, :, None]  # Dim TxZxDx1
                mu = torch.as_tensor(self.posterior_mean(self.t_obs), dtype=torch.float).permute(2, 0, 1)[..., None]  # Dim TxZxDx1
                cov = torch.as_tensor(self.posterior_cov(self.t_obs), dtype=torch.float).permute(3, 0, 1, 2)  # Dim TxZxDxD
                q_z = torch.as_tensor(self.posterior_mjp(self.t_obs), dtype=torch.float).permute(1, 0)  # Dim TxZ
                N = len(self.t_obs)

                if Sigma_chol.grad is not None:
                    Sigma_chol.grad.zero_()

                loss = -0.5 * N * Sigma_logdet - 0.5 * (q_z * (quad_form(x - mu, Sigma_inv) + (Sigma_inv * cov).sum((2, 3)))).sum()
                loss.backward()

                gradient = Sigma_chol.grad

                while elbo_new < elbo_old and n_iter <= max_iter:
                    scaled_gradient = decay ** n_iter * gradient

                    Sigma_chol_new = Sigma_chol_cur - scaled_gradient

                    self.obs_cov_chol = Sigma_chol_new
                    self.update_posterior()
                    elbo_new = self.elbo()
                    n_iter += 1

                if elbo_new < elbo_old and n_iter == max_iter + 1:
                    self.obs_cov_chol = Sigma_chol_cur
                    self.update_posterior()
                    terminated = True

                if (elbo_new - elbo_old).abs() / elbo_old.abs() < epsrel:
                    break
                elif not terminated:
                    elbo_old = deepcopy(elbo_new)
                    elbo_new = torch.tensor(-float('inf'))
                    n_steps += 1
                    n_iter = 1

    # ---------- 2.2) Diffusion prior updates ---------------
    def update_prior_diffusion(self, max_steps: int, max_iter: int, decay: float, epsrel_elbo:float, epsrel_int: float):
        """
         Updates the parameters of the prior Diffusion process

         :param epsrel numerical accuracy for update_prior_slope and update_prior_intercept --> see documentation update_prior_slope, update_prior_intercept
        """
        logging.debug('Updating Prior Diffusion Parameters')
        self.update_diffusion_prior_initial_conditons()
        self.update_prior_slope(max_steps=max_steps, max_iter=max_iter, decay=decay, epsrel_elbo=epsrel_elbo, epsrel_int=epsrel_int)
        self.update_prior_intercept(epsrel=epsrel_int)

    def update_diffusion_prior_initial_conditons(self):
        """
        Updates the prior diffusion model initial conditons (mu_p(z,0), Sigma_p(z,0))
        """

        self.initial_prior_mean = self.initial_posterior_mean.detach().clone()
        self.initial_prior_cov = self.initial_posterior_cov.detach().clone()

    def update_prior_slope(self, max_steps: int, max_iter: int, decay: float, epsrel_elbo: float, epsrel_int: float):
        """
        Update the prior_slope closed-form (MLE).

        :param epsrel: Numerical accuracy when evaluating the intergrals for E and C matrices
        """

        def _C_integrand(t: float):
            q_z = self.posterior_mjp(t)[:, None, None]  # Dimension ZxNxN
            Sigma = self.posterior_cov(t)  # Dimension #ZxNxN
            mu = self.posterior_mean(t)[..., None]  # Dimension ZxNxN
            mu_T = np.moveaxis(mu, -2, -1)  # Dimension ZxNxN
            return q_z * (Sigma + mu @ mu_T)  # Dimension #ZxNxN

        def _E_integrand(t: float):
            q_z = self.posterior_mjp(t)[:, None, None]  # Dimension ZxNxN
            A_q = self.variational_slope(t)  # Dimension ZxNxN
            Sigma = self.posterior_cov(t)  # Dimension ZxNxN
            mu = self.posterior_mean(t)[..., None]  # Dimension ZxNxN
            mu_T = np.moveaxis(mu, -2, -1)  # Dimension ZxNxN
            b_bar = self.variational_intercept(t)[..., None] - self.prior_intercept.numpy()[
                ..., None]  # Dimension ZxNxN

            return q_z * A_q @ (Sigma + mu @ mu_T) + b_bar @ mu_T  # Dimension ZxNxN

        elbo_old = self.elbo()
        elbo_new = torch.tensor(-float('inf'))
        n_steps = 1
        n_iter = 1
        terminated = False
        while n_steps <= max_steps and terminated is False:

            C = torch.as_tensor(quad_vec_intervals(_C_integrand, self.interval_boundaries, epsrel=epsrel_int)[0],
                                dtype=torch.float)  # Dimension ZxNxN

            E = torch.as_tensor(quad_vec_intervals(_E_integrand, self.interval_boundaries, epsrel=epsrel_int)[0],
                                dtype=torch.float)  # Dimension ZxNxN

            A_p_cur = deepcopy(self.prior_slope)
            A_p = self.prior_slope
            D_i = self.prc_cov_inv.detach()

            gradient = D_i @ (A_p @ C - E)

            while elbo_new < elbo_old and n_iter <= max_iter:
                scaled_gradient = decay ** n_iter * gradient

                A_p_new = A_p_cur - scaled_gradient

                self.prior_slope = A_p_new
                self.update_posterior()
                elbo_new = self.elbo()
                n_iter += 1

            if elbo_new < elbo_old and n_iter == max_iter + 1:
                self.prior_slope = A_p_cur
                self.update_posterior()
                terminated = True

            if (elbo_new - elbo_old).abs() / elbo_old.abs() < epsrel_elbo:
                break
            elif not terminated:
                elbo_old = deepcopy(elbo_new)
                elbo_new = torch.tensor(-float('inf'))
                n_steps += 1
                n_iter = 1

    def update_prior_intercept(self, epsrel: float):
        """
        Update the prior_intercept closed-form (MLE).

        :param epsrel: Numerical accuracy when evaluating the intergral for B matrix
        """
        expected_posterior_sojourn_times = self._expected_posterior_sojourn_times[:, None]  # Dimension Z x N

        def _B_integrand(t: float):
            q_z = self.posterior_mjp(t)[:, None]  # Dimension ZxN
            A_q = self.variational_slope(t)  # Dimension ZxNxN
            A_p = self.prior_slope.numpy()  # Dimension ZxNxN
            A_bar = A_q - A_p  # Dimension ZxNxN
            b_q = self.variational_intercept(t)[..., None]  # Dimension ZxNxN
            mu = self.posterior_mean(t)[..., None]  # Dimension ZxNxN

            return q_z * (A_bar @ mu + b_q).squeeze(-1)  # Dimension ZxN

        B = torch.as_tensor(quad_vec_intervals(_B_integrand, self.interval_boundaries, epsrel=epsrel)[0],
                            dtype=torch.float)

        self.prior_intercept = B / expected_posterior_sojourn_times  # Dimension Z x N

    # ---------- 2.3) MJP prior updates ---------------
    def update_prior_mjp(self):
        """
         Updates the parameters of the prior MJP
        """
        logging.debug('Updating Prior MJP Parameters')
        self.update_mjp_prior_initial_conditons()
        self.update_prior_rate_mtx()

    def update_mjp_prior_initial_conditons(self):
        """
        Updates the mjp initial conditons of the prior p(z,0)
        """

        self.initial_prior_mjp = self.initial_posterior_mjp.detach().clone()

    def update_prior_rate_mtx(self):
        """
        Update the prior MJP rate matrix with the closed-form MLE, cf. appendix_notes Sect. 4.1

        :return: None
        """
        expected_sojourn_times = self._expected_posterior_sojourn_times
        expected_rates = self._expected_posterior_rates

        prior_rate_mtx = expected_rates.double() / expected_sojourn_times.double()[:, None]
        torch.diagonal(prior_rate_mtx, dim1=-1, dim2=-2)[:] = 0.0

        diagonal = -1.0 * prior_rate_mtx.sum(1)
        torch.diagonal(prior_rate_mtx, dim1=-1, dim2=-2)[:] = diagonal

        self.prior_rate_mtx = prior_rate_mtx

    # --------- 2.4) Prior dispersion update ----------
    def update_prior_dispersion(self, max_steps: int, max_iter: int, decay: float, epsrel_int: float,
                                epsrel_elbo: float, mode_specific_dispersion: bool):
        """
        Update the process covariance (dispersion) self.prc_cov.

        :param max_iter: int specifying the maximum number of gradient steps
        :param epsrel: convergence criteria --> relative change in loss
        """
        logging.debug('Updating Prior Dispersion')
        # TODO not until convergence, but with max_iter like with variational_slope and _intercept?

        def _C_integrand(t: float):
            A_bar = torch.as_tensor(self.variational_slope(t), dtype=torch.float) - self.prior_slope  # Dimension ZxNxN'
            A_bar_T = A_bar.permute(0, 2, 1)  # Dim ZxN'xN
            b_bar = (torch.as_tensor(self.variational_intercept(t), dtype=torch.float) - self.prior_intercept)[
                ..., None]  # Dimension ZxNx1
            b_bar_T = b_bar.permute(0, 2, 1)  # Dim Zx1xN
            mu = torch.as_tensor(self.posterior_mean(t)[..., None], dtype=torch.float)  # Dimension ZxNxN'
            mu_T = mu.permute(0, 2, 1)  # Dim ZxN'xN
            Sigma = torch.as_tensor(self.posterior_cov(t), dtype=torch.float)  # Dimension ZxN'xN
            q_z = torch.as_tensor(self.posterior_mjp(t), dtype=torch.float)[..., None, None]  # Dimension Zx1x1

            A_mu_b_T = A_bar @ (mu @ b_bar_T)  # Dim ZxNxN'
            A_mu_b_T_T = A_mu_b_T.permute(0, 2, 1)  # Dim ZxN'xN

            return q_z * (quad_form(A_bar_T, mu @ mu_T + Sigma, keepdims=True) + A_mu_b_T_T + A_mu_b_T + b_bar @ b_bar_T)

        def _Psi_integrand(t: float):
            Psi = self.cov_costate(t)  # Dim ZxDxD
            return Psi

        elbo_old = self.elbo()
        elbo_new = torch.tensor(-float('inf'))
        n_steps = 1
        n_iter = 1
        terminated = False
        while n_steps <= max_steps and terminated is False:

            C = torch.as_tensor(quad_vec_intervals(_C_integrand, self.interval_boundaries, epsrel=epsrel_int)[0],
                                dtype=torch.float)  # Dimension ZxDxD

            Psi = torch.as_tensor(quad_vec_intervals(_Psi_integrand, self.interval_boundaries, epsrel=epsrel_int)[0],
                                  dtype=torch.float)  # Dim ZxDxD

            D_cur = self.prc_cov.detach().clone()  # Dim ZxDxD

            D_inv = self.prc_cov_inv  # Dim ZxDxD

            gradient = -0.5 * D_inv @ C @ D_inv + Psi
            if mode_specific_dispersion:
                pass
            else:
                gradient = gradient.sum(0)[None, ...]

            while elbo_new < elbo_old and n_iter <= max_iter:
                scaled_gradient = decay ** n_iter * gradient

                D_new = D_cur - scaled_gradient

                try:
                    self.prc_cov = D_new
                    self.update_posterior()
                    elbo_new = self.elbo()
                    n_iter += 1
                except:
                    n_iter += 1

            if elbo_new < elbo_old and n_iter == max_iter + 1:
                self.prc_cov = D_cur
                self.update_posterior()
                terminated = True

            if (elbo_new - elbo_old).abs() / elbo_old.abs() < epsrel_elbo:
                break
            elif not terminated:
                elbo_old = deepcopy(elbo_new)
                elbo_new = torch.tensor(-float('inf'))
                n_steps += 1
                n_iter = 1


    # --------------------------------------------
    # ---------- Compute ELBO --------------------
    # --------------------------------------------
    def elbo(self, **kwargs) -> torch.Tensor:
        """
        Return the evidence lower bound for the given set of parameters.

        :param `**kwargs`: kwargs that-if given-replace the corresponding parameters of self. Possible kws are all
                           parameters of VSHSSMF, i.e.,
                           posterior_mjp : VariationalMJP
                           posterior_mean: ReshapedOdeSolution,
                           posterior_cov: ReshapedOdeSolution,
                           prior_slope: torch.Tensor
                           prior_intercept: torch.Tensor
                           variational_slope: VariationalSlope
                           variational_intercept: VariationalIntercept
                           prc_cov_inv: torch.Tensor
                           obs_cov: torch.Tensor
                           obs_data: torch.Tensor
                           prior_rate_mtx: torch.Tensor
                           mjp_costate: ReshapedOdeSolution
        :return: torch Tensor (scalar): the ELBO
        """
        if len(kwargs) == 0:
            vssde = self
        else:
            vssde = deepcopy(self)
            for k, val in kwargs.items():
                setattr(vssde, k, val)

        return vssde._elbo()

    def _elbo(self, rtol=1e-3, maxiter=200) -> torch.Tensor:
        """
        Return the evidence lower bound for the parameters specified in the model.

        :return: torch Tensor (scalar): the ELBO
        """

        # 1. Compute Expected log likelihood
        expected_loglike = self._expected_loglikelihood()

        # 2. Compute initial KL + KL over time
        kl_initial = self._kl_initial_conditions()  # Add initial conditions

        kl_path = torch.as_tensor(
            quadrature_intervals(lambda t: self._kl_integrand(t).numpy(), self.interval_boundaries, rtol=rtol,
                                 maxiter=maxiter)[0],
            dtype=torch.float)

        return expected_loglike - kl_initial - kl_path

    def _kl_initial_conditions(self) -> torch.Tensor:
        """
        Compute the  kl divergence between variational posterior and prior initial conditions
        :return: kl divergence
        """
        # Compute KL(q_z || p_z)
        kl_mjp = discrete_kl(self.initial_posterior_mjp.detach(), self.initial_prior_mjp)  # scalar

        # Compute KL(q(y|z) || p(y|z))
        kl_diffusion_z = gaussian_kl(mu_q=self.initial_posterior_mean.detach(),
                                     Sigma_q=self.initial_posterior_cov.detach(),
                                     mu_p=self.initial_prior_mean, Sigma_p=self.initial_prior_cov)  # Dimension Z

        # Compute E_{q_z}[KL(q(y|z) || p(y|z))]
        q_z = self.initial_posterior_mjp.detach()  # Dimension Z
        kl_diffusion = torch.sum(q_z * kl_diffusion_z)  # scalar

        return kl_diffusion + kl_mjp

    def _kl_integrand(self, t) -> torch.Tensor:
        """
        Compute the integrand of the KL diveregence between the posterior and prior SSDE process
        # See Apendix notes Sec 1.1 and 1.2

        :return: scalar tensor or [T] dimensional tensor kl_integrand at T time point(s) t
        """
        t = torch.as_tensor(t)

        if t.ndim == 0:

            # res_1, 1/2 * E[h^T D^-1 h], cf. appendix_notes Sect. 1.2
            A_bar = torch.as_tensor(self.variational_slope(t), dtype=torch.float) - self.prior_slope  # Dimension ZxNxN'
            b_bar = (torch.as_tensor(self.variational_intercept(t), dtype=torch.float) - self.prior_intercept)[
                ..., None]  # Dimension ZxNx1
            mu = torch.as_tensor(self.posterior_mean(t)[..., None], dtype=torch.float)  # Dimension ZxN'xN
            Sigma = torch.as_tensor(self.posterior_cov(t), dtype=torch.float)  # Dimension ZxN'xN
            D_i = self.prc_cov_inv.detach()  # Dimension ZxN'xN
            q_z = torch.as_tensor(self.posterior_mjp(t), dtype=torch.float)  # Dimension Z
            variational_log_rate_mtx = torch.as_tensor(self.variational_log_rate_mtx(t),
                                                       dtype=torch.float)  # Dimension ZxZ'
            variational_rate_mtx = torch.as_tensor(self.variational_rate_mtx(t), dtype=torch.float)
            prior_rate_mtx = self.prior_rate_mtx.float()  # Dimension ZxZ'

            drift_diff = A_bar @ mu + b_bar  # Dimension ZxNxN
            quadratic_form = quad_form(drift_diff, D_i)  # Dimension Z
            trace = torch.sum(A_bar * D_i @ A_bar @ Sigma, (-1, -2))  # Dimension Z

            kl_integrand_diffusion = 0.5 * torch.sum(q_z * (trace + quadratic_form))  # scalar

            # res_2, \sum_z \sum_z'\neq z q(z, t) \bar{Lambda}_zz'
            Lambda_bar = variational_rate_mtx * (
                    variational_log_rate_mtx - torch.log(prior_rate_mtx) - 1.0) + prior_rate_mtx  # Dimension Zx Z'
            torch.diagonal(Lambda_bar, dim1=-2, dim2=-1)[:] = 0.0

            kl_integrand_mjp = torch.sum(q_z[:, None] * Lambda_bar)  # scalar

        else:

            # res_1, 1/2 * E[h^T D^-1 h], cf. appendix_notes Sect. 1.2
            A_bar = torch.as_tensor(self.variational_slope(t), dtype=torch.float).permute(3, 0, 1, 2) - \
                    self.prior_slope[None, ...]  # Dimension TxZxNxN
            b_bar = torch.as_tensor(self.variational_intercept(t), dtype=torch.float).permute(2, 0, 1)[..., None] - \
                    self.prior_intercept[None, ..., None]  # Dimension TxZxNx1
            mu = torch.as_tensor(self.posterior_mean(t), dtype=torch.float).permute(2, 0, 1)[
                ..., None]  # Dimension TxZxN'xN
            Sigma = torch.as_tensor(self.posterior_cov(t), dtype=torch.float).permute(3, 0, 1, 2)  # Dimension TxZxN'xN
            D_i = self.prc_cov_inv.detach()[None, ...]  # Dimension TxZxNxN'
            q_z = torch.as_tensor(self.posterior_mjp(t), dtype=torch.float).permute(1, 0)  # Dimension TxZ
            variational_log_rate_mtx = torch.as_tensor(self.variational_log_rate_mtx(t), dtype=torch.float).permute(2,
                                                                                                                    0,
                                                                                                                    1)  # Dimension TxZxZ'
            variational_rate_mtx = torch.as_tensor(self.variational_rate_mtx(t), dtype=torch.float).permute(2, 0,
                                                                                                            1)  # Dim TxZxZ'

            prior_rate_mtx = self.prior_rate_mtx[None, ...].float()  # Dimension TxZxZ'

            drift_diff = A_bar @ mu + b_bar  # Dimension TxZxNxN
            quadratic_form = quad_form(drift_diff, D_i)  # Dimension TxZ
            trace = torch.sum(A_bar * D_i @ A_bar @ Sigma, dim=(-1, -2))  # Dimension TxZ

            kl_integrand_diffusion = 0.5 * torch.sum(q_z * (trace + quadratic_form), dim=-1)  # Dimension T

            # res_2, \sum_z \sum_z'\neq z q(z, t) \bar{Lambda}_zz'
            Lambda_bar = variational_rate_mtx * (variational_log_rate_mtx - torch.log(
                prior_rate_mtx) - 1.0) + prior_rate_mtx  # Dimension TxZx Z'
            torch.diagonal(Lambda_bar, dim1=-2, dim2=-1)[:] = 0.0  # Dimension TxZ

            kl_integrand_mjp = torch.sum(q_z[..., None] * Lambda_bar, dim=(-2, -1))  # Dimension T

        return kl_integrand_diffusion + kl_integrand_mjp

    def _expected_loglikelihood(self) -> torch.Tensor:
        """
        Computes the expected log likelihood E[log p(x|y)]

        :return: tensor [1,], expected log likelihood
        """

        Sigma_obs_i = self.obs_cov_inv.detach()[None, None, ...]  # Dimension TxZxNxN
        Sigma_obs_logdet = self.obs_cov_logdet.detach()

        x = self.obs_data.permute(1, 0)[:, None, :]  # Dimension TxZxN
        mu_q = torch.as_tensor(self.posterior_mean(self.t_obs), dtype=torch.float).permute(2, 0, 1)  # Dimension TxZxN
        Sigma_q = torch.as_tensor(self.posterior_cov(self.t_obs), dtype=torch.float).permute(3, 0, 1,
                                                                                             2)  # Dimension TxZxNxN
        q_z = torch.as_tensor(self.posterior_mjp(self.t_obs), dtype=torch.float).permute(1, 0)  # Dimension TxZ

        expected_loglike = -.5 * (
                self.n_dim * torch.log(2 * torch.as_tensor(np.pi, dtype=torch.float)) + Sigma_obs_logdet + (
                q_z * (quad_form(x - mu_q, Sigma_obs_i) + torch.sum(Sigma_obs_i * Sigma_q, dim=(-1, -2)))).sum(
            dim=-1)).sum()

        return expected_loglike

    # ------------------------------------------------------------
    # -------------- Sampling------------------------------------
    # ------------------------------------------------------------
    def sample(self, process: str, n_trajectories: int = 1, observation_times: Union[List, np.ndarray] = None,
               dt: float = 1e-3, n_jobs: int = 1):
        """
        Return a Trajectory object holding sampled trajectories from prior or posterior.

        :param process: String, can be 'prior' or 'posterior'
        :param observation_times: If given, synthetic observations at these time points will be generated
        :param n_trajectories: Int specifying the number of samples.
        :param dt: Float, the time step used for Euler-Maruyama.
        :param n_jobs: Int, number of parallel jobs (-> joblib.Parallel)
        :return: np.ndarray
        """
        if observation_times is not None:
            if isinstance(observation_times, np.ndarray):
                assert observation_times.shape[0] == n_trajectories, 'Number of trajectories must match number of provided observation time point arrays'
                if n_trajectories > 1:
                    observation_times = list(observation_times)
                else:
                    observation_times = [observation_times]
            else:
                if n_trajectories > 1:
                    assert len(
                        observation_times) == n_trajectories, 'Number of trajectories must match number of provided observation time point arrays'
                else:
                    assert isinstance(observation_times[0],
                                      float), 'Number of trajectories must match number of provided observation time point arrays'
                    observation_times = [observation_times]

        jump_times, mjp_paths, mjp_paths_discretized = self.sample_mjp(process, n_trajectories, dt=dt, n_jobs=n_jobs)
        time_grid, sde_paths = self.sample_conditional_diffusion(process, n_trajectories, mjp_paths_discretized, dt,
                                                                 n_jobs)

        if observation_times is None:
            observations = None
        else:
            observations = self.sample_observations(time_grid, sde_paths, observation_times)

        trajectory = Trajectory(jump_times, mjp_paths, time_grid, mjp_paths_discretized, sde_paths,
                                observation_times, observations)
        return trajectory

    # ----------- Gillespie sampling of discrete-valued trajectories
    def sample_mjp(self, process: str, n_trajectories: int, dt: float, n_jobs: int = 1) -> Tuple[List, List, Any]:
        """
        Gillespie sampling.
        # TODO update doc

        :param process: String, can be 'prior' or 'posterior'
        :param n_trajectories: Integer specifying the number of trajectories to be drawn
        :param dt: float, time discretization
        :param n_jobs: int, number of parallel jobs
        :return: tuple, (trajectories, jump_times)
        """
        state_trajectories = []
        jump_times = []

        if process == 'prior':
            res = Parallel(n_jobs=n_jobs)(delayed(self._sample_prior_mjp)() for i in range(n_trajectories))
        elif process == 'posterior':
            res = Parallel(n_jobs=n_jobs)(delayed(self._sample_posterior_mjp)() for i in range(n_trajectories))
        else:
            raise AttributeError

        for elem in res:
            jump_times.append(elem[0])
            state_trajectories.append(elem[1])

        state_trajectories_discretized = self._discretize_mjp_trajectory(jump_times, state_trajectories, dt)

        return jump_times, state_trajectories, state_trajectories_discretized

    # TODO doc
    def _sample_prior_mjp(self) -> Tuple[List, List]:
        trajectory = []
        jump_times = []
        t_start, t_end = self.t_span

        z_init = np.random.choice(np.arange(self.n_states), p=self.initial_prior_mjp)
        trajectory.append(z_init)
        t_cur = t_start
        while t_cur < t_end:
            z_cur = trajectory[-1]
            Q_xy = self.prior_rate_mtx_zerodiag[z_cur].numpy()
            Q_xx = -1 * np.sum(Q_xy)

            sojourn_time = np.random.exponential(
                -1 * 1 / Q_xx)  # Note: the numpy version uses shape parameterization
            t_next = t_cur + sojourn_time
            z_next = np.random.choice(np.arange(self.n_states), p=-1 * Q_xy / Q_xx)

            if t_next <= t_end:
                trajectory.append(z_next)
                jump_times.append(t_next)
            t_cur = t_next

        return jump_times, trajectory

    # TODO doc
    def _sample_posterior_mjp(self) -> Tuple[List, List]:
        trajectory = []
        jump_times = []
        t_start, t_end = self.t_span
        t_query_rate = np.linspace(t_start, t_end, 1000)

        z_init = np.random.choice(np.arange(self.n_states), p=self.initial_prior_mjp)
        trajectory.append(z_init)
        t_cur = t_start
        while t_cur < t_end:
            # 1. Set upper bound (overall maxmimum rate from the current time point onwards)
            t_cur_idx = np.where(t_query_rate >= t_cur)[0][0]
            future_posterior_rate_mtxs = self.variational_rate_mtx(t_query_rate[t_cur_idx:])  # Dimension ZxZ'xT
            # Get diagonal view
            future_exit_rates = np.einsum('iij->ij', future_posterior_rate_mtxs)  # Dimension ZxT

            lambda_star = np.amax(np.abs(future_exit_rates))

            # 2. Sample inter-arrival time
            u = np.random.uniform()
            tau = -1 * np.log(u) / lambda_star

            # 3. Update current time
            t_cur += tau

            # Get instantaneous rate at new time
            posterior_rate_mtx = self.variational_rate_mtx(t_cur)  # Dimension ZxZ'
            # Get diagonal view
            exit_rates = np.einsum('jj->j', posterior_rate_mtx)  # Dimension Z

            lambda_t = np.abs(exit_rates[trajectory[-1]])

            # Compare instantaneous to maximum rate and accept the jump time with probability lambda_t/lambda_star
            s = np.random.uniform()
            if s <= lambda_t / lambda_star:
                jump_times.append(t_cur)

                # find next state
                transition_probabilities = (np.eye(self.n_states) + posterior_rate_mtx / lambda_t)[trajectory[-1]]
                z_next = np.random.choice(np.arange(self.n_states), p=transition_probabilities)
                trajectory.append(z_next)

        return jump_times, trajectory

    def _discretize_mjp_trajectory(self, jump_times, state_trajectories, dt):
        n_trajectories = len(state_trajectories)

        timesteps = np.arange(self.t_span[0], self.t_span[1] + dt, dt)
        n_timesteps = timesteps.shape[0]
        state_paths_discretized = np.zeros((n_trajectories, n_timesteps), dtype=int)
        for n in range(n_trajectories):
            insertion_indices = np.searchsorted(timesteps, jump_times[n])
            indices = np.concatenate((np.array([self.t_span[0]]), insertion_indices, np.array([self.t_span[1]])))
            for pair_idx, pair in enumerate(zip(indices[:-1], indices[1:])):
                state_paths_discretized[n, pair[0]:pair[1]] = int(state_trajectories[n][pair_idx])

        return state_paths_discretized

    # ----------- Euler-Maruyama sampling of continuous-valued trajectories
    # Todo doc
    def sample_conditional_diffusion(self, process: str, n_trajectories: int, discrete_trajectories: np.ndarray,
                                     dt: float,
                                     n_jobs: int = 1):
        time = np.arange(self.t_span[0], self.t_span[1] + dt, dt)

        res = Parallel(n_jobs=n_jobs)(
            delayed(self._conditional_euler_maruyama)(process, discrete_trajectories[n], time, dt) for n in
            range(n_trajectories))

        return time, np.array(res)

    # TODO doc
    def _conditional_euler_maruyama(self, process: str, discrete_trajectory: np.ndarray, time: np.ndarray, dt: float):
        continuous_trajectory = np.zeros((self.n_dim, len(discrete_trajectory)))

        z_0 = discrete_trajectory[0]
        if process == 'prior':
            y_0 = np.random.multivariate_normal(self.initial_prior_mean[z_0].numpy(),
                                                self.initial_prior_cov[z_0].numpy())
            drift = self._prior_drift
        elif process == 'posterior':
            y_0 = np.random.multivariate_normal(self.initial_posterior_mean[z_0].detach().numpy(),
                                                self.initial_posterior_cov[z_0].detach().numpy())
            drift = self._posterior_drift
        else:
            raise TypeError

        continuous_trajectory[:, 0] = y_0

        if self.n_dim == 1:
            dW = np.random.normal(scale=np.sqrt(dt), size=(len(time)))
            dW = dW[:, None]
        else:
            dW = np.random.multivariate_normal(np.zeros(self.n_dim), dt * np.eye(self.n_dim), size=(len(time)))

        for i in range(1, len(time)):
            t_cur = time[i - 1]
            z_cur = discrete_trajectory[i - 1]
            y_cur = continuous_trajectory[:, i - 1]
            y_next = (y_cur + drift(y_cur, z_cur, t_cur) * dt + self.prc_cov_chol[z_cur].detach().numpy() @ dW[
                i])
            continuous_trajectory[:, i] = y_next

        return continuous_trajectory

    def _prior_drift(self, y, z, t):
        return self.prior_slope[z, :, :].numpy() @ y + self.prior_intercept[z, :].numpy()

    def _posterior_drift(self, y, z, t):
        return self.variational_slope(t)[z, :, :] @ y + self.variational_intercept(t)[z, :]

    # ------------------- Generate noisy observations ------------
    def sample_observations(self, time_grid, sde_paths, observation_times):
        n_samples = sde_paths.shape[0]

        observations = []
        for n in range(n_samples):
            time_grid_indices = np.searchsorted(time_grid, observation_times[n])
            y = np.moveaxis(sde_paths[n, :, time_grid_indices], 1, 0)
            observations.append(self._observe(y))

        return observations

    def _observe(self, y: np.ndarray) -> np.ndarray:
        """
        Return noisy observations for latent Y-values y.

        :param y: [n_dim, n_observations] array, values of latent Y-process at observation time points
        :return: np.ndarray
        """
        if self.n_dim == 1:
            observations = np.random.normal(loc=y, scale=np.sqrt(self.obs_cov.detach()))
        else:
            res = []
            n_samples = y.shape[-1]
            # TODO parallelize
            for i in range(n_samples):
                res.append(np.random.multivariate_normal(y[:, i], self.obs_cov.detach()))
            observations = np.array(res)

        return observations

    # ------------------------------------------------------------
    # ----------- Posterior sampling
    # ------------------------------------------------------------

    def posterior_marginal(self, t: Union[float, torch.Tensor]) -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor]:
        """
        Returns the posterior marginal of the VSHSSMF process for time point(s) t
        q(y,z,t)=q(z,t)q(y,t|z)=q(z,t) N(y|mu(z,t),Sigma(z,t))

        :param t: single or multiple time points to evaluate the posterior marginal
        :return: q(z,t) mjp posterior marginal(s),
                 mu(z,t) mean of diffusion posterior marginal(s),
                 Sigma(z,t) cov of diffusion posterior marginal(s)
        """

        t = np.asarray(t)
        q_z = torch.as_tensor(self.posterior_mjp(t), dtype=torch.float)
        mu = torch.as_tensor(self.posterior_mean(t), dtype=torch.float)
        Sigma = torch.as_tensor(self.posterior_cov(t), dtype=torch.float)
        return q_z, mu, Sigma


# ---------- Classes for variational posterior parameters ----------------
class VariationalSlope:
    def __init__(self,
                 prior_slope: np.ndarray,
                 t_span: Iterable = None,
                 t_obs: np.ndarray = None,
                 n_interpolants: int = None,
                 interpolants: np.ndarray = None,
                 time_grid: np.ndarray = None,
                 slope_clip: float = None):  # TODO doc and extend to higher dimensions
        """
        Class for a variational slope given by the update formula A_q=A_p + 2 * D * Psi / q_z

        :param prior_slope: numpy array containing the prior slope A_p
        :param prc_cov:  numpy array containing the process covariance D
        :param cov_costate: ReshapedOdeSolution containing the covariance costate dynamics Psi(t)
        :param posterior_mjp: VariationalMJP containing the MJP marginals q_z(t)
        """
        if interpolants is not None:
            assert n_interpolants is None
            assert time_grid is not None
            _interpolants = np.moveaxis(interpolants, 3, 0)
            n_dim = _interpolants.shape[-1]
            n_states = _interpolants.shape[1]
            eval, evec = np.linalg.eig(_interpolants)
            eval.real[:] = np.clip(eval.real, a_min=None, a_max=slope_clip)
            D = np.array([[np.eye(n_dim) for n in range(n_states)]for m in _interpolants])
            D = D * eval[..., None]
            #evec_T = np.moveaxis(evec, 3, 2)
            evec_I = np.linalg.inv(evec)
            _interpolants = (evec @ D @ evec_I).real
            _interpolants = np.moveaxis(_interpolants, 0, 3)
            self._interpolants = _interpolants
            self.time_grid = time_grid
        else:
            assert interpolants is None
            assert time_grid is None
            assert t_obs is not None
            self.time_grid = np.linspace(t_span[0], t_span[1], n_interpolants)
            self.time_grid = np.sort(np.append(self.time_grid, t_obs))
            self._interpolants = np.zeros((*prior_slope.shape, self.time_grid.shape[-1]))  # Dim ZxNxN'xT
            self._interpolants[..., :] = prior_slope[..., None]  # Dim ZxNxN'xT

        _interpolants = self._interpolants.reshape((-1, self._interpolants.shape[-1]), order='F')
        self.interpolant_fn = interpolate.interp1d(self.time_grid, _interpolants, kind='previous', bounds_error=False,
                                                   fill_value=(_interpolants[:, 0], _interpolants[:, -1]))

    @property
    def interpolants(self):
        return self._interpolants

    def gradient(self,
                 prc_cov_inv: np.ndarray,
                 prior_slope: torch.Tensor,
                 prior_intercept: torch.Tensor,
                 variational_intercept,
                 posterior_mean: ReshapedOdeSolution,
                 posterior_cov: ReshapedOdeSolution,
                 posterior_mjp: ReshapedOdeSolution,
                 mean_costate: ReshapedOdeSolution,
                 cov_costate: ReshapedOdeSolution):

        t = self.time_grid

        D_inv = prc_cov_inv[None, ...]  # Dimension TxZxNxN'
        Psi = np.transpose(cov_costate(t), axes=(3, 0, 1, 2))  # Dimension TxZxN'xN
        q_z = np.transpose(posterior_mjp(t), axes=(1, 0))[..., None, None]  # Dimension TxZxNxN'
        A_bar = np.moveaxis(self._interpolants, 3, 0) - prior_slope[None, ...]  # Dimension TxZxNxN
        b_bar = np.moveaxis(variational_intercept(t), 2, 0)[..., None] - prior_intercept[None, ..., None]  # Dimension TxZxNx1
        mu = np.moveaxis(posterior_mean(t), 2, 0)[..., None]  # Dimension TxZxN'xN
        mu_T = np.moveaxis(mu, 3, 2)  # Dimension TxZxN'xN
        lambda_ = np.transpose(mean_costate(t), axes=(2, 0, 1))[..., None]  # Dimension TxZxN'xN
        Sigma = np.moveaxis(posterior_cov(t), 3, 0)  # Dimension TxZxN'xN

        mu_muT = mu * mu_T
        lambda_muT = lambda_ * mu_T

        # Calculate conditional expectation E_{y|z}[f(y, z, t) y^T]
        _A_grad = q_z * D_inv @ (A_bar @ (mu_muT + Sigma) + b_bar * mu_T) + 2 * Psi @ Sigma + lambda_muT
        _A_grad = np.moveaxis(_A_grad, 0, 3)
        return _A_grad

    def __call__(self, t: Any):
        """
        Evaluates the slope according to A_q=A_p + 2 * D * Psi / q_z

        :param t: time point(s) for evaluation can be float, singleton or array of size n_times
        :return: [n_states x n_dim x n_dim] or [n_states x n_dim x n_dim x n_times] variational slope at time point(s) t
        """

        # Cast as numpy
        t = np.asarray(t)

        # interpolate the _A array
        res = self.interpolant_fn(t).astype(float)

        if t.ndim == 0:
            res = res.reshape(self._interpolants.shape[:-1], order='F')
        else:
            res = res.reshape((*self._interpolants.shape[:-1], *t.shape), order='F')
        return res


class VariationalIntercept:
    def __init__(self,
                 prior_intercept: np.ndarray,
                 t_span: Iterable = None,
                 t_obs: np.ndarray = None,
                 n_interpolants: int = None,
                 interpolants: np.ndarray = None,
                 time_grid: np.ndarray = None):  # TODO doc
        """
        Class for a variational intercept given by the update formula b_q= b_p-A_bar mu + D * lambda / q_z

        :param prior_slope: numpy array containing the prior slope A_p
        :param prior_intercept: numpy array containing the prior intercept b_p
        :param prc_cov: numpy array containing the process covariance D
        :param variational_slope: VariationalSlope containing the variational slope A_q(t)
        :param mean_costate: ReshapedOdeSolution containing the mean costate dynamics lambda_(t)
        :param posterior_mjp: VariationalMJP containing the MJP marginals q_z(t)
        :param posterior_mean: ReshapedOdeSolution containing  the posterior mean dynamics mu(t)
        """
        if interpolants is not None:
            assert n_interpolants is None
            assert time_grid is not None
            self._interpolants = interpolants
            self.time_grid = time_grid
        else:
            assert interpolants is None
            assert time_grid is None
            assert t_obs is not None
            self.time_grid = np.linspace(t_span[0], t_span[1], n_interpolants)
            self.time_grid = np.sort(np.append(self.time_grid, t_obs))
            self._interpolants = np.zeros((*prior_intercept.shape, self.time_grid.shape[-1]))  # Dim ZxNxT
            self._interpolants[..., :] = prior_intercept[..., None]  # Dim ZxNxT

        _interpolants = self._interpolants.reshape((-1, self._interpolants.shape[-1]), order='F')
        self.interpolant_fn = interpolate.interp1d(self.time_grid, _interpolants, bounds_error=False, kind='previous',
                                                   fill_value=(_interpolants[:, 0], _interpolants[:, -1]))

    @property
    def interpolants(self):
        return self._interpolants

    def __call__(self, t: Any):
        """
         Evaluates the intercept according to b_q= b_p-A_bar mu + D * lambda / q_z
         :param t: time point(s) for evaluation can be float, singleton or array of size n_times
         :return: [n_states x n_dim ] or [n_states x n_dim x n_times] variational slope at time point(s) t
        """

        # Cast as numpy
        t = np.asarray(t)

        # interpolate the _b array
        res = self.interpolant_fn(t).astype(float)

        if t.ndim == 0:
            res = res.reshape(self._interpolants.shape[:-1], order='F')
        else:
            res = res.reshape((*self._interpolants.shape[:-1], *t.shape), order='F')
        return res

    def gradient(self,
                 prc_cov_inv: np.ndarray,
                 prior_slope: torch.Tensor,
                 prior_intercept: torch.Tensor,
                 variational_slope,
                 posterior_mean: ReshapedOdeSolution,
                 posterior_mjp: ReshapedOdeSolution,
                 mean_costate: ReshapedOdeSolution):

        t = self.time_grid

        D_inv = prc_cov_inv[None, ...]  # Dimension TxZxNxN'
        q_z = np.transpose(posterior_mjp(t), axes=(1, 0))[..., None, None]  # Dimension TxZxNxN'
        A_bar = np.moveaxis(variational_slope(t), 3, 0) - prior_slope[None, ...]  # Dimension TxZxNxN
        b_bar = np.moveaxis(self._interpolants, 2, 0)[..., None] - prior_intercept[None, ..., None]  # Dimension TxZxNx1
        mu = np.moveaxis(posterior_mean(t), 2, 0)[..., None]  # Dimension TxZxNx1
        lambda_ = np.transpose(mean_costate(t), axes=(2, 0, 1))[..., None]  # Dimension TxZxNx1

        _b_grad = (q_z * D_inv @ (A_bar @ mu + b_bar) + lambda_).squeeze(-1)
        _b_grad = np.moveaxis(_b_grad, 0, 2)

        return _b_grad


class VariationalLogRateMtx:
    def __init__(self,
                 prior_rate_mtx_zerodiag: np.ndarray,
                 t_span: Iterable = None,
                 t_obs: np.ndarray = None,
                 n_interpolants: int = None,
                 interpolants: np.ndarray = None,
                 time_grid: np.ndarray = None,
                 clip_above: float = None):
        """
        Class for a variational rate matrix given by the update formula #TODO
        """
        if interpolants is not None:
            assert n_interpolants is None
            assert time_grid is not None
            self._interpolants = np.clip(interpolants, a_min=None, a_max=clip_above)
            self.time_grid = time_grid
        else:
            assert interpolants is None
            assert time_grid is None
            assert t_obs is not None
            self.time_grid = np.linspace(t_span[0], t_span[1], n_interpolants)
            self.time_grid = np.sort(np.append(self.time_grid, t_obs))

            # Initialize values on this time grid; set diagonal to an arbitrary value
            prior_rate_mtx_zerodiag = deepcopy(prior_rate_mtx_zerodiag)
            diag = np.einsum('ii->i', prior_rate_mtx_zerodiag)
            diag[:] = 1
            self._interpolants = np.zeros((*prior_rate_mtx_zerodiag.shape, self.time_grid.shape[-1]))  # Dim ZxZ'xT
            self._interpolants[..., :] = np.log(prior_rate_mtx_zerodiag)[..., None]  # Dim ZxZ'xT

        _interpolants = self._interpolants.reshape((-1, self._interpolants.shape[-1]), order='F')
        self.interpolant_fn = interpolate.interp1d(self.time_grid, _interpolants, kind='previous', bounds_error=False,
                                                   fill_value=(_interpolants[:, 0], _interpolants[:, -1]))

    @property
    def interpolants(self):
        return self._interpolants

    def __call__(self, t: Any):
        """
         Evaluates the rate matrix according to #TODO

        :param t: time point(s) for evaluation can be float, singleton or array of size n_times
        :return: [n_states x n_states] or [n_states x n_states x n_time_points] posterior rate matrix
        """

        # Cast as numpy
        t = np.asarray(t)

        # interpolate the array
        res = self.interpolant_fn(t)

        if t.ndim == 0:
            res = res.reshape(self._interpolants.shape[:-1], order='F')
        else:
            res = res.reshape((*self._interpolants.shape[:-1], *t.shape), order='F')
        return res

    def gradient(self,  # TODO doc
                 prior_rate_mtx: np.ndarray,
                 posterior_mjp: ReshapedOdeSolution,
                 mjp_costate: ReshapedOdeSolution):
        '''
        Returns the gradient of the log variational rate matrix with 0's on the diagonal.

        :param prior_rate_mtx:
        :param mjp_costate:
        :return:
        '''

        t = self.time_grid

        nu = mjp_costate(t)  # Dim ZxT
        q_z = posterior_mjp(t)  # Dim ZxT

        prior_rate_mtx = deepcopy(prior_rate_mtx)[..., None]  # Dim ZxZ'xT
        # Set diagonal to some positive value (which does not matter, since we dismiss the diagonal at the end)
        diag = np.einsum('iit->it', prior_rate_mtx)
        diag[:] = 1.

        _logQ_grad = q_z[:, None, :] * np.exp(self._interpolants) * (
                self._interpolants - np.log(prior_rate_mtx) - nu[:, None, :] + nu[None, ...])

        # Get diagonal view
        diag = np.einsum('iit->it', _logQ_grad)
        diag[:] = 0

        return _logQ_grad


# TODO Doc
class Trajectory:

    def __init__(self, jump_times: List, mjp_paths: List, time_grid: np.ndarray, mjp_paths_discretized: np.ndarray,
                 sde_paths: np.ndarray, observation_times: Union[List, np.ndarray] = None,
                 observations: Union[List, np.ndarray] = None):
        self.jump_times = jump_times
        self.mjp = mjp_paths
        self.time_grid = time_grid
        self.mjp_discretized = mjp_paths_discretized
        self.sde = sde_paths

        if observation_times is not None:
            self.observation_times = observation_times

        if observations is not None:
            self.observations = observations

        self.t_span = [self.time_grid[0], self.time_grid[-1]]
        self.n_trajs = len(self.jump_times)

    def plot_one_1D_traj_with_obs(self, i: int = None):

        if i is None:
            i = np.random.choice(range(self.n_trajs))

        fig, ax = plt.subplots(2, sharex=True, gridspec_kw={'hspace': 0, 'height_ratios': [1, 3]}, figsize=(5, 5))

        event_times = [self.t_span[0]] + self.jump_times[i] + [self.t_span[1]]
        ax[0].step(event_times, self.mjp[i] + [self.mjp[i][-1]], where='post', lw=1.5,
                   c='b')  # NOTE: the '+ [Z.sample_paths[-1]] is ONLY due to .step, because it apparently needs start- and end-point.
        ax[0].set_ylabel('$Z$', rotation=0, fontsize='x-large')
        ax[0].yaxis.set_label_coords(-0.05, 0.35)

        ax[1].plot(self.time_grid, self.sde[i][0], lw=1, c='g')
        ax[1].set_ylabel('$Y\mid Z$', rotation=0, fontsize='x-large')
        ax[1].yaxis.set_label_coords(-0.05, 0.45)

        ax[1].scatter(self.observation_times[i], self.observations[i], marker='x', s=60, c='r', label='observations')
        ax[1].set_xlabel('Time', fontsize='x-large')
        ax[1].set_ylabel('$X\mid Y$', rotation=0, fontsize='x-large')

        return
